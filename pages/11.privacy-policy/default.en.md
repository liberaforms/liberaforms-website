---
title: 'Privacy Policy'
visible: false
slug: privacy-policy
---

# LiberaForms Privacy Policy

LiberaForms is particularly aware of the protection of personal data of users of Internet services. We are looking for the best way for users to regain control over their data and remain anonymous when they prefer.

Likewise, at LiberaForms we strive to comply as much as possible with the General Data Protection Regulation (GDRP), the California Consumer Privacy Act (CCPA) and other privacy regulations. We know it's your data, and it's important to us.

In this policy, we detail what data we collect and for what we use it for, how this information is treated and what your rights are regarding your data on:

* Our website [http://www.liberaforms.org](http://www.liberaforms.org/en) and its complementary websites ([blog.liberaforms.org](http://blog.liberaforms.org), [docs.liberaforms.org](http://docs.liberaforms.org), etc.) and communication channels (e-newsletter).
* Our public instances under the liberaforms.org domain, listed on the main page of [http://www.liberaforms.org](http://www.liberaforms.org/en).

We promise not to do business with your data, ever.

## 1\. Responsible for data processing

LiberaForms is a collective initiative supported by the free software community for the development of ethical online forms. You can learn more about the project at <https://www.liberaforms.org/en/project>.

The legal representative is Estraperlo S. Coop. of C-LM with registered office at Carrer Matadero, 5 – 02.260 Fuentealbilla (Spain, European Union) with tax identification number F-02546489.

All data is on servers hosted entirely in Europe.

## 2\. Data protection

The personal data provided by the users will be treated with the appropriate degree of protection, according to the current reference regulations, with the necessary security measures to avoid their alteration, loss, treatment or unauthorized access by third parties.

Instead, the data that the user manages directly through their forms will be governed by their own privacy policy and under their responsibility.

LiberaForms protects access to your personal data with the technical measures at our disposal. Thus, most of the processes can be self-managed, meaning that the LiberaForms technical team will only access the maintenance part of the services, but not the data, unless the User explicitly requests that an intervention be made in their service

### 2\.1. Visit our website

The privacy of visitors to our websites is important to us. We do not do individual tracking. As a visitor or anonymous user on our websites:

* No personal information is collected.
* No information is shared, sent to or sold to third parties.
* No information is exploited to identify personal and behavioral trends.
* Information is not monetized.

### Cookies

We only use our own and essential cookies.

On the [www.liberaforms.org](http://www.liberaforms.org/en) website there are technical cookies of a temporary nature to ensure the functionality of the website. No personal information is stored in these web browser cookies.

The sites [blog.liberaforms.org](http://blog.liberaforms.org) and [docs.liberaforms.org](http://docs.liberaforms.org), do not use cookies.

In public instances of the LiberaForms application, a single session cookie is used. This is generated when the user logs into one of the instances mentioned, and is used to identify the user account and associated services. This cookie is temporary and only remains until you log out, close your browser or turn off your device.

### Web analytics

We use [Plausible Analytics](https://plausible.io/) to collect some anonymous usage data for statistical purposes. The goal is to track general trends in website traffic and not to track individual visitors. All data is aggregated. No personal data is stored.

The data collected includes referral sources, main pages, visit duration, device information (device type, operating system, country and browser) used during the visit. You can see all [the details in the Plausible Analytics data policy](https://plausible.io/data-policy).

### 2\.2. Subscription to the newsletter

For people who choose to follow the project and receive information via email, we use a a self-hosted instance of [ListMonk](https://listmonk.app/) to deliver updates directly to your inbox. All emails are sent using the [Maadix](https://maadix.net/en/) email service provider.

Personal information requested are a name, an email address and the language desired for communication.

The legal basis is the user's consent. You can unsubscribe from our newsletter at any time by following the instructions at the bottom of the emails you receive.

### 2\.3. User registration in the LiberaForms service

Our guiding principle is to collect only what you we need and will only process this information to provide you with the service for which you have registered.

If you choose to create an account in one of the LiberaForms instances, we will ask you for some minimum information: username, email and password.

This information enables the maintenance, development and management of the service relationship for the procurement of products or services through this website. The data processed for this purpose will be kept as long as this relationship is maintained and, once it has ended, during the periods of retention and limitation of liability provided for by law.

Here's what that means in practice:

### What we collect and the purpose we use it for

An email address is required to create an account on the public instances of LiberaForms. This is just so you can log in and use the service.

All data is on servers hosted entirely in Europe. This ensures that all data, both your personal data, and forms and responses, is covered by strict European Union data privacy laws. Your site data never leaves the EU.

All transactional emails are sent using the [Maadix](https://maadix.net/en/) email service provider.

LiberaForms will not access the data that the User stores on our server, unless the user requests explicit intervention.

The legal basis is the execution of the agreement to provide the service to the User. You can choose to delete your LiberaForms account at any time.

### Using the Privacy Assistant

The use of our optional assistant requires each registered User to provide additional information for it's correct use.

On form creation, when the Privacy Assistant is active, a contact channel (an email account that can be personal) of the registered user's privacy officer, and an (optional) URL with the privacy policy  applying to the form are requested. This data is used to inform anonymous users in the process of responding to a form or later.

### Service communications

To keep the user informed, we may send you timely service information or other essential information to your mailbox.

We use our own self-hosted instance of [ListMonk](https://listmonk.app/) to send these updates directly to your inbox. All emails are sent using the [Maadix](https://maadix.net/en/) email provider.

The legal basis is the legitimate interest to provide the service in the best conditions, therefore we process the personal data of the users. This is not a newsletter, but communications related to the service and are necessary for the correct provision of the service.

### Other considerations

Forms are public documents. When creating a form if you include any personal information (addresses, telephone numbers...). If that information is of third parties, make sure you have the corresponding consent or legitimacy. In any case, it will be under your responsibility and LiberaForms is not involved.

### 2\.4 Data conservation

To determine the data retention period for the User's personal data, we use the following criteria:

• Personal data included in the answers to the forms, if any, indefinitely, as long as the user does not delete his account or the answers in question are deleted.

• Personal data obtained through consent: until consent is withdrawn.

• Personal data received or created during the contractual relationship: while the execution of the contract lasts.

• Personal Data obtained when contacting us only for a query: during the time necessary to attend to the query.

• Personal contact data limited to name, telephone number and address, e-mail: indefinitely as long as the User does not request their deletion.

• Personal data necessary to fulfill our legal or regulatory obligations, as well as to administer our rights (for example, to assert our claims before the courts) or for statistical or historical purposes: the time necessary to be able to carry out the fulfillment of obligations, the claim, etc.

When we no longer need to use the User's personal data, we will delete them from our files. Please note that there is a 3-month grace period for the final deletion of data included in existing backups.

### 2\.5 Transfer of data

There is no transfer of data outside the LiberaForms environment, neither your data nor that of your forms and their responses.

### 2\.6 International transfers

There is no transfer of data outside the LiberaForms environment, neither your data nor that of your forms and their responses.

### 2\.7 Your rights

* If you have provided your data on our website or public instances, you can request that a file be sent to you with the personal data we have stored about you. You can also delete your account or request that we delete any of your personal data. This does not include any data that we are required to retain for administrative, legal or security reasons.
* The User may at any time exercise the rights of access, rectification, cancellation, opposition, limitation or portability by email addressed through the email [info@liberaforms.org](mailto:info@liberaforms.org)
* In such cases where legitimacy is based on consent, the User has the right to withdraw this consent at any time.
* In the event of a breach of personal data, we will notify, within a maximum period of 72 hours from the moment we become aware of it. The breach of personal data will be reported to the competent supervisory authority, unless it is unlikely that the breach of the personal data pose a risk to the rights and freedoms of natural persons.

We remind you that in any case, you have the right to submit a claim to your local data protection authority.

## 3\. Changes and questions

We may update this policy as necessary to comply with relevant regulations and to reflect any new practices. Whenever we make a significant change to our policies, we will also announce it on [our project blog](https://blog.liberaforms.org/) or on our social media profiles.

Contact us at [info@liberaforms.org](mailto:info@liberaforms.org) if you have any questions, comments, or concerns about this privacy policy, your data, or your rights regarding your information.

*Last update: August 30, 2023.*