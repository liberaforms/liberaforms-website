---
title: 'Política de privacidad'
visible: false
slug: politica-de-privacidad
---

# Política de privacidad de LiberaForms
LiberaForms está especialmente sensibilizada en la protección de datos de carácter personal de las personas usuarias de los servicios de Internet. Buscamos la mejor manera para que las usuarias puedan recuperar el control sobre sus datos y mantener el anonimato cuando así lo prefieran.

Igualmente, en LiberaForms hacemos un esfuerzo por cumplir en la medida de lo posible con el General Data Protection Regulationa (GDRP), la California Consumer Privacy Act (CCPA) y otras regulaciones de privacidad. Sabemos que son tus datos y para nosotros son importantes.

En esta política te detallamos que datos recogemos y para qué uso, como se trata dicha información y cuáles son tus derechos sobre tus datos sobre:

* Nuestro sitio web [www.liberaforms.org](http://www.liberaforms.org/es) y sus sitios web complementarios (blog.liberaforms.org, docs.liberaforms.org, etc.) y canales de comunicación (boletín electrónico).
* Nuestras instancias públicas bajo dominio liberaforms.org listadas en la página principal de [www.liberaforms.org](http://www.liberaforms.org/es).

Nos comprometemos a no hacer negocio con tus datos, nunca.

## 1. Responsable del tratamiento de los datos
LiberaForms es una iniciativa colectiva que se apoya en la comunidad de software libre para el desarrollo de formularios éticos en línea. Puedes saber más sobre el proyecto en [https://www.liberaforms.org/es/proyecto](https://www.liberaforms.org/es/proyecto)

El representante legal es Estraperlo S. Coop. de C-LM con domicilio social en la calle Matadero, 5 – 02.260 Fuentealbilla (España, Unión Europea) con identificación fiscal F-02546489.

Todos los datos se encuentran en servidores alojados íntegramente en Europa.

## 2. Protección de los datos

Los datos de carácter personal proporcionados por las personas usuarias serán tratados con el grado de protección adecuado, según la normativa vigente de referencia, con las medidas de seguridad necesarias para evitar su alteración, pérdida, tratamiento o acceso no autorizado por terceros.

En cambio, los datos que gestione la persona usuaria directamente a través de sus formularios se regirán por su propia política de privacidad y bajo su responsabilidad.

LiberaForms intenta reducir el acceso a tus datos personales con las medidas técnicas a nuestra disposición. Así, la mayor parte de los procesos podrán autogestionarse, de forma que las personas técnicas de LiberaForms solo accederán a la parte de mantenimiento de los servicios, pero no a los datos, salvo que la Usuaria reclame explícitamente que se realice una intervención en su servicio.

### 2.1. Visita a nuestro sitio web
La privacidad de los visitantes a nuestros sitios web es importante para nosotros. No hacemos seguimiento individual. Como visitante o usuario anónimo a nuestros sitios web:

* No se recopila información personal.
* No se comparte, envía o vende información a terceros.
* No hay información explotada para identificar tendencias personales y de comportamiento.
* No se monetiza la información.

### Cookies
Únicamente utilizamos cookies propias y que sean indispensables.

En el sitio web de www.liberaforms.org existen cookies técnicas de carácter temporal para garantizar la funcionalidad del sitio web. No se almacena información personal en estas cookies del navegador web.

En los sitios docs.liberaforms.org y blog.liberaforms.org no se utilizan cookies.

En las instancias públicas de la aplicación LiberaForms, se utiliza una única cookie de sesión. Esta se genera cuando la persona usuaria se registra en una de dichas instancias. Se utiliza para identificar la cuenta de usuario y sus servicios asociados. Este cookie es de carácter temporal y únicamente se mantiene mientras no cierres la sesión, cierres el navegador o apagues el dispositivo.

### Analítica web
Utilizamos [Plausible Analytics](https://plausible.io) para recopilar alhttps://plausible.io/gunos datos de uso anónimos con fines estadísticos. El objetivo es el seguimiento de las tendencias generales en el tráfico del sitio web, y no es el seguimiento de los visitantes individuales. Todos los datos están en agregado. No se conserva ningún dato personal.

Los datos recopilados incluyen fuentes de referencia, páginas principales, duración de la visita, información de los dispositivos (tipo de dispositivo, sistema operativo, país y navegador) utilizados durante la visita. Puedes ver todos los detalles en [la política de datos de Plausible Analytics](https://plausible.io/data-policy).

### 2.2 Suscripción al boletín informativo

Para los seguidores del proyecto que eligen la opción de recibir información por correo electrónico, usamos una instancia propia de [ListMonk](https://listmonk.app) para enviar dichas novedades directamente a tu buzón. Todos los correos electrónicos se envían utilizando el proveedor de correo electrónico de [Maadix](https://maadix.net/es).

Los datos que se solicitan son un nombre, una dirección de correo y el idioma deseado para la comunicación.

La base legal es el consentimiento de la persona usuaria. En cualquier momento puedes darte de baja de nuestro boletín informativo siguiendo las instrucciones al pie de los correos recibidos.

### 2.3. Registro de usuarios en el servicio de LiberaForms

Nuestro principio rector es recopilar solo lo que necesitamos y solo procesaremos esta información para brindarte el servicio para el que te has registrado.

Si optas por crear una cuenta en alguna de las instancias de LiberaForms, te pediremos unos mínimos datos: usuario, correo electrónico y contraseña.

Dicha información posibilita el mantenimiento, desarrollo y gestión de la relación de servicio por la contratación de productos o servicios a través de este sitio web. Los datos tratados a tal fin se conservarán mientras se mantenga esta relación y, una vez finalizada, durante los plazos de conservación y prescripción de responsabilidad legalmente previstos.

Esto es lo que eso significa en la práctica:

### Qué recopilamos y para qué lo usamos

Se requiere una dirección de correo electrónico para crear una cuenta en las instancias públicas de LiberaForms. Eso es solo para que puedas iniciar sesión y hacer uso del servicio.

Todos los datos se encuentran en servidores alojados íntegramente en Europa. Esto garantiza que todos los datos, tanto tus datos personales como tus formularios y sus respuestas, están cubiertos por las estrictas leyes de la Unión Europea sobre privacidad de datos. Los datos de su sitio nunca salen de la UE.

Todos los correos electrónicos transaccionales se envían utilizando el proveedor de correo electrónico de  [Maadix](https://maadix.net/es).

LiberaForms no accederá a los datos que la Usuaria almacene en nuestro servidor, salvo que la persona usuaria solicite una intervención explicita.

La base legal es la ejecución del acuerdo para prestar el servicio a la Usuaria. Puedes optar por eliminar tu cuenta de LiberaForms en cualquier momento.

### Uso del Asistente de privacidad
El uso de nuestro asistente, opcional, requiere de información adicional que cada Usuaria registrada facilita para un buen funcionamiento de dicha funcionalidad.

En la creación del formulario, cuando está activo el Asistente de privacidad, se pide un canal de contacto (una cuenta de correo que puede ser personal) con el responsable de privacidad del usuario registrado y una URL (opcional) con la política de privacidad que ampara dicho formulario. Estos datos se utilizan para poder informar a las usuarias anónimas en el proceso de responder a un formulario o más adelante.

### Comunicaciones de servicio

Con el objetivo de mantener informada a la persona usuaria, podemos enviarte de forma puntual información de servicio u otra información esencial a tu buzón.

Usamos una instancia propia de [ListMonk](https://listmonk.app) para enviar dichas novedades directamente a tu buzón. Todos los correos electrónicos se envían utilizando el proveedor de correo electrónico de  [Maadix](https://maadix.net/es).

La base legal es el interés legítimo en poder prestar el servicio en las mejores condiciones, por lo que necesitamos tratar los datos personas de las personas usuarias. No se trata de un boletín informativo, sino de comunicaciones relacionadas con el servicio y son necesarias para la correcta prestación del servicio.

### Otras consideraciones

Los formularios son documentos públicos. Si incorporas cualquier información personal (direcciones, números de teléfono...), tuya o de terceros, en la creación de un formulario, asegúrate de tener el consentimiento o legitimidad correspondiente. En todo caso, será bajo tu responsabilidad y queda al margen LiberaForms.

### 2.4 Conservación de datos

Para determinar el periodo de retención de datos de los datos personales de la Usuaria, utilizamos los siguientes criterios:

• Datos Personales incluidos en las respuestas a los formularios, si los hubiera, indefinidamente, mientras la persona usuaria no elimine su cuenta o se eliminen las respuestas en cuestión.

• Datos Personales obtenidos mediante consentimiento: hasta la retirada del consentimiento.

• Datos Personales recibidos o creados durante la relación contractual: mientras dure la ejecución del contrato.

• Datos Personales obtenidos al contactar con nosotros solo para una consulta: durante el tiempo necesario para atender a la consulta.

• Datos Personales de contacto limitados a nombre, número de teléfono y dirección, correo electrónico: indefinidamente mientras la Usuaria no nos solicite su eliminación.

• Datos Personales necesarios para cumplir con nuestras obligaciones legales o reglamentarias, así como para administrar nuestros derechos (por ejemplo, para hacer valer nuestras reclamaciones ante los tribunales) o con finalidades estadísticas o históricas: el tiempo necesario para poder llevar a cabo el cumplimiento de obligaciones, la reclamación, etc.

Cuando ya no necesitamos utilizar los datos personales de la Usuaria, los eliminaremos de nuestros archivos. Ten en cuenta que existe un margen de 3 meses para el borrado definitivo de los datos incluidos en las copias de seguridad existentes.

### 2.5 Cesión de datos

No hay cesión de datos alguna fuera del entorno de LiberaForms, ni tus datos, ni los de tus formularios y sus respuestas.

### 2.6 Transferencias internacionales

No hay transferencias de datos alguna fuera del entorno de LiberaForms, ni tus datos, ni los de tus formularios y sus respuestas.

### 2.7 Tus derechos

* Si has proporcionado tus datos en nuestro sitio web o instancias públicas puedes solicitar que se te envíe un archivo con los datos personales que tenemos almacenados sobre ti. También puedes eliminar tu cuenta o solicitar que borremos cualquier dato personal tuyo. Esto no incluye ningún dato que estemos obligados a conservar por motivos administrativos, legales o de seguridad.
* La Usuaria podrá ejercer en cualquier momento los derechos de acceso, rectificación, cancelación, oposición, limitación o portabilidad mediante correo electrónico dirigido a través del correo electrónico [info@liberaforms.org](mailto:info@liberaforms.org).
* En los tratamientos cuya legitimación se base en el consentimiento, la Usuaria tiene derecho a retirar este consentimiento en cualquier momento. 
* En caso de violación de los datos personales, notificaremos en un plazo máximo de 72 horas a partir del momento en que tengamos conocimiento de ella, la violación de los datos personales a la autoridad de supervisión competente, a menos que sea improbable que la violación de los datos personales suponga un riesgo para los derechos y libertades de las personas físicas.
Te recordamos que en cualquier caso tienes derecho a presentar una reclamación ante tu autoridad local referente en protección de datos.

## 3. Cambios y preguntas

Podemos actualizar esta política según sea necesario para cumplir con las regulaciones pertinentes y reflejar cualquier práctica nueva. Cada vez que hagamos un cambio significativo en nuestras políticas, también lo anunciaremos en [el blog de nuestro proyecto](https://blog.liberaforms.org) o en nuestros perfiles de las redes sociales.

Contáctenos en  [info@liberaforms.org](mailto:info@liberaforms.org) si tienes alguna pregunta, comentario o inquietud sobre esta política de privacidad, tus datos o tus derechos con respecto a tu información.

_Última actualización: 30 de agosto de 2023_