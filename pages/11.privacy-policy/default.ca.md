---
title: 'Política de privadesa'
published: true
metadata:
    keywords: 'termes del servei, condicions d''ús, LiberaForms'
    description: 'Condicions d''ús del servei SaaS de LiberaForms'
slug: politica-de-privacitat
visible: false
---

# Política de privadesa de LiberaForms
LiberaForms està especialment sensibilitzada en la protecció de dades de caràcter personal de les persones usuàries dels serveis d'Internet. Busquem la millor manera perquè les usuàries puguin recuperar el control sobre les dades i mantenir l'anonimat quan així ho prefereixin.

Igualment, a LiberaForms fem un esforç per complir en la mesura del possible amb el General Data Protection Regulation (GDRP), la Califòrnia Consumer Privacy Act (CCPA) i altres regulacions de privadesa. Sabem que són les teves dades i per a nosaltres són importants.

En aquesta política et detallem quines dades recollim i per a quin ús, com es tracta aquesta informació i quins són els teus drets sobre les teves dades sobre:

* El nostre lloc web [http://www.liberaforms.org](http://www.liberaforms.org/ca) i els seus llocs web complementaris ([blog.liberaforms.org](http://blog.liberaforms.org), [docs.liberaforms.org](http://docs.liberaforms.org), etc.) i canals de comunicació (butlletí electrònic).
* Les nostres instàncies públiques sota domini liberaforms.org llistades a la pàgina principal de [http://www.liberaforms.org](http://www.liberaforms.org/ca).

Ens comprometem a no fer negoci amb les teves dades, mai.

## 1. Responsable del tractament de les dades

LiberaForms és una iniciativa col·lectiva que es recolza a la comunitat de programari lliure per al desenvolupament de formularis ètics en línia. Pots saber més sobre el projecte a [https://www.liberaforms.org/ca/projecte](https://www.liberaforms.org/ca/projecte).

El representant legal és Estraperlo S. Coop. de C-LM amb domicili social al carrer Matadero, 5 – 02.260 Fuentealbilla (Espanya, Unió Europea) amb identificació fiscal F-02546489.

Totes les dades es troben a servidors allotjats íntegrament a Europa.

## 2. Protecció de les dades

Les dades de caràcter personal proporcionades per les persones usuàries seran tractades amb el grau de protecció adequat, segons la normativa vigent de referència, amb les mesures de seguretat necessàries per evitar la seva alteració, pèrdua, tractament o accés no autoritzat per tercers.

En canvi, les dades que gestioni la persona usuària directament a través dels seus formularis es regiran per la seva pròpia política de privadesa i sota la seva responsabilitat.

LiberaForms intenta reduir l'accés a les teves dades personals amb les mesures tècniques a la nostra disposició. Així, la major part dels processos podran autogestionar-se, de manera que les persones tècniques de LiberaForms només accediran a la part de manteniment dels serveis, però no a les dades, llevat que la Usuària reclami explícitament que es faci una intervenció en el seu servei.

### 2.1. Visita al nostre lloc web

La privadesa dels visitants als nostres llocs web és important per a nosaltres. No fem seguiment individual. Com a visitant o usuari anònim als nostres llocs web:

* No es recopila informació personal.
* No es comparteix, envia o ven informació a tercers.
* No hi ha informació explotada per identificar tendències personals i de comportament.
* No es monetitza la informació.

### Galetes

Únicament utilitzem galetes pròpies i que siguin indispensables.

Al lloc web de [www.liberaforms.org](http://www.liberaforms.org/ca) hi ha galetes tècniques de caràcter temporal per garantir la funcionalitat del lloc web. No s'emmagatzema informació personal en aquestes galetes del navegador web.

Als llocs [blog.liberaforms.org](http://blog.liberaforms.org) i [docs.liberaforms.org](http://docs.liberaforms.org) no s'utilitzen galetes.

A les instàncies públiques de l'aplicació LiberaForms, s'utilitza una única galeta de sessió. Aquesta es genera quan la persona usuària es registra en una de les instàncies esmentades. S'utilitza per identificar el compte d'usuari i els serveis associats. Aquesta galeta és de caràcter temporal i únicament es manté mentre no tanquis la sessió, tanquis el navegador o apaguis el dispositiu.

### Analítica web
Utilitzem [Plausible Analytics](https://plausible.io/) per recopilar algunes dades d'ús anònimes amb finalitats estadístiques. L'objectiu és el seguiment de les tendències generals en el trànsit del lloc web i no el seguiment dels visitants individuals. Totes les dades estan en agregat. No es conserva cap dada personal.

Les dades recopilades inclouen fonts de referència, pàgines principals, durada de la visita, informació dels dispositius (tipus de dispositiu, sistema operatiu, país i navegador) utilitzats durant la visita. Podeu veure tots els detalls a [la política de dades de Plausible Analytics](https://plausible.io/data-policy).

### 2.2 Subscripció al butlletí informatiu

Per als seguidors del projecte que trien l'opció de rebre informació per correu electrònic, fem servir una instància pròpia de [ListMonk](https://listmonk.app/) per enviar aquestes novetats directament a la teva bústia. Tots els correus electrònics s'envien fent servir el proveïdor de correu electrònic de [Maadix](https://maadix.net/ca/).

Les dades que se sol·liciten són un nom, una adreça de correu i l'idioma desitjat per a la comunicació.

La base legal és el consentiment de la persona usuària. En qualsevol moment et pots donar de baixa del nostre butlletí informatiu seguint les instruccions al peu dels correus rebuts.

### 2.3. Registre d'usuaris en el servei de LiberaForms

El nostre principi rector és recopilar només allò que necessitem i només processarem aquesta informació per brindar-te el servei per al qual t'has registrat.

Si optes per crear un compte en alguna de les instàncies de LiberaForms, et demanarem unes dades mínimes: usuari, correu electrònic i contrasenya.

Aquesta informació possibilita el manteniment, el desenvolupament i la gestió de la relació de servei per la contractació de productes o serveis a través d'aquest lloc web. Les dades tractades amb aquesta finalitat es conservaran mentre es mantingui aquesta relació i, un cop finalitzada, durant els terminis de conservació i prescripció de responsabilitat legalment previstos.

Això és el que això significa a la pràctica:

### Què recopilem i per què ho fem servir

Cal una adreça de correu electrònic per crear un compte a les instàncies públiques de LiberaForms. Això és només perquè puguis iniciar sessió i fer ús del servei.

Totes les dades es troben a servidors allotjats íntegrament a Europa. Això garanteix que totes les dades, tant les teves dades personals com els formularis i les respostes, estan cobertes per les estrictes lleis de la Unió Europea sobre privadesa de dades. Les dades del vostre lloc mai surten de la UE.

Tots els correus electrònics transaccionals s'envien utilitzant el proveïdor de correu electrònic de [Maadix](https://maadix.net/ca/).

LiberaForms no accedirà a les dades que la Usuària emmagatzemi al nostre servidor, llevat que la persona usuària sol·liciti una intervenció explicita.

La base legal és l'execució de l'acord per prestar el servei a la Usuària. Pots optar per eliminar el teu compte de LiberaForms en qualsevol moment.

### Ús de l'Assistent de privadesa

L'ús del nostre assistent, opcional, requereix informació addicional que cada Usuària registrada facilita per a un bon funcionament d'aquesta funcionalitat.

En la creació del formulari, quan està actiu l'Assistent de privadesa, es demana un canal de contacte (un compte de correu que pot ser personal) amb el responsable de privadesa de l'usuari registrat i un URL (opcional) amb la política de privadesa que empara aquest formulari. Aquestes dades es fan servir per poder informar les usuàries anònimes en el procés de respondre a un formulari o més endavant.

### Comunicacions de servei

Amb l'objectiu de mantenir informada la persona usuària, podem enviar-te de manera puntual informació de servei o altra informació essencial a la teva bústia.

Fem servir una instància pròpia de [ListMonk](https://listmonk.app/) per enviar aquestes novetats directament a la teva bústia. Tots els correus electrònics s'envien fent servir el proveïdor de correu electrònic de [Maadix](https://maadix.net/ca/).

La base legal és l'interès legítim de poder prestar el servei en les millors condicions, per la qual cosa necessitem tractar les dades persones de les persones usuàries. No es tracta d'un butlletí informatiu, sinó de comunicacions relacionades amb el servei i són necessàries per a la prestació correcta del servei.

### Altres consideracions

Els formularis són documents públics. Si incorpores qualsevol informació personal (adreces, números de telèfon...), teva o de tercers, a la creació d'un formulari, assegura't de tenir el consentiment o la legitimitat corresponent. En tot cas, serà sota la teva responsabilitat i en queda al marge LiberaForms.

### 2.4 Conservació de dades

Per determinar el període de retenció de dades de les dades personals de la Usuària, utilitzem els criteris següents:

• Dades personals incloses en les respostes als formularis, si n'hi hagués, indefinidament, mentre la persona usuària no elimini el seu compte o s'eliminin les respostes en qüestió.

• Dades personals obtingudes mitjançant consentiment: fins a la retirada del consentiment.

• Dades personals rebudes o creades durant la relació contractual: mentre duri l'execució del contracte.

• Dades Personals obtingudes en contactar amb nosaltres només per a una consulta: durant el temps necessari per atendre la consulta.

• Dades Personals de contacte limitades a nom, número de telèfon i adreça, correu electrònic: indefinidament mentre la Usuària no ens sol·liciti la seva eliminació.

• Dades personals necessàries per complir les nostres obligacions legals o reglamentàries, així com per administrar els nostres drets (per exemple, per fer valer les nostres reclamacions davant dels tribunals) o amb finalitats estadístiques o històriques: el temps necessari per poder dur a terme el compliment d'obligacions, la reclamació, etc.

Quan ja no necessitem utilitzar les dades personals de la Usuària, les eliminarem dels nostres arxius. Tingues en compte que hi ha un marge de 3 mesos per a l'esborrat definitiu de les dades incloses a les còpies de seguretat existents.

### 2.5 Cessió de dades
No hi ha cap cessió de dades fora de l'entorn de LiberaForms, ni les teves dades, ni les dels teus formularis i les seves respostes.

### 2.6 Transferències internacionals
No hi ha cap transferència de dades fora de l'entorn de LiberaForms, ni les teves dades, ni les dels teus formularis i les seves respostes.

### 2.7 Els teus drets
* Si has proporcionat les teves dades al nostre lloc web o instàncies públiques pots sol·licitar que se t'enviï un fitxer amb les dades personals que tenim emmagatzemades sobre tu. També pots eliminar el teu compte o sol·licitar que esborrem qualsevol dada personal teva. Això no inclou cap dada que estiguem obligats a conservar per motius administratius, legals o de seguretat.
* La Usuària podrà exercir en qualsevol moment els drets d'accés, rectificació, cancel·lació, oposició, limitació o portabilitat mitjançant correu electrònic dirigit a través del correu electrònic [info@liberaforms.org](mailto:info@liberaforms.org).
* En els tractaments la legitimació dels quals es basen en el consentiment, la Usuària té dret a retirar aquest consentiment en qualsevol moment. 
* En cas de violació de les dades personals notificarem, en un termini màxim de 72 hores a partir del moment en què en tinguem coneixement, la violació de les dades personals a l'autoritat de supervisió competent, llevat que sigui improbable que la violació de les dades personals suposi un risc per als drets i les llibertats de les persones físiques.
Et recordem que en qualsevol cas tens dret a presentar una reclamació davant de la teva autoritat local referent en protecció de dades.

### 3. Canvis i preguntes

Podem actualitzar aquesta política segons calgui per complir amb les regulacions pertinents i reflectir qualsevol pràctica nova. Cada vegada que fem un canvi significatiu en les nostres polítiques, també ho anunciarem [al bloc del nostre projecte](https://blog.liberaforms.org/) o als nostres perfils de les xarxes socials.

Contacta amb nosaltres a [info@liberaforms.org](mailto:info@liberaforms.org) si tens alguna pregunta, comentari o inquietud sobre aquesta política de privadesa, les teves dades o els teus drets respecte a la teva informació.

_Darrera actualització: 30 d'agost del 2023._
