---
title: Pribatutasun-politika
visible: false
slug: pribatutasun-politika
---

# LiberaFormsen pribatutasun-politika
LiberaForms bereziki sentsibilizatua dago interneteko zerbitzuetako erabiltzaileen datu pertsonalak babestean. Erabiltzaileek beren datuen gaineko kontrola berreskuratu eta hala nahi dutenean anonimo izateko modu hoberena bilatzen dugu.

Era berean, LiberaFormsen, pribatutasuna arautzen duten hainbat lege betetzen saiatzen gara, hala nola Datuen Babeserako Erregelamendu Orokorra (DBEO) eta California Consumer Privacy Act (CCPA). Argi dugu zure datuak zureak direla eta hori garrantzitsua da guretzat.

Politika honetan, zehazten dugu zer datu biltzen ditugun eta zertarako, nola tratatzen den informazio hori eta zeintzuk diren honako webgune hauetako zure datuen gainean dituzun eskubideak:

* Gure webgune nagusia, [www.liberaforms.org](http://www.liberaforms.org/es), eta horren webgune osagarriak (blog.liberaforms.org, docs.liberaforms.org, etab.), eta baita ere komunikazio kanalak (buletin elektronikoa).
* liberaforms.org domeinupean ditugun instantzia publikoak, [www.liberaforms.org](http://www.liberaforms.org/es) webguneko orri nagusian zerrendatzen direnak.

Hitzematen dugu ez dugula zure datuekin negoziorik egingo, inoiz ere ez.

## 1. Datu-tratamenduaren arduraduna
LiberaForms ekimen kolektibo bat da, software libreko komunitatearen laguntza duena internet bidezko galdetegi etikoak garatzeko. Proiektuari buruz gehiago jakiteko sartu [https://liberaforms.org/eu/proiektua](https://www.liberaforms.org/es/proyecto) helbidean

Legezko ordezkaria Estraperlo S. Coop. Gaztela-Mantxakoa da, egoitza soziala Calle Matadero, 5 – 02.260 Fuentealbilla (Espainia) helbidean duena eta identifikazio fiskala F-02546489.

Datu guztiak oso-osorik Europan ostatatutako zerbitzarietan daude.

## 2. Datuen babesa

Erabiltzaileek ematen dituzten datu pertsonalak babes-maila egokiaz tratatuko dira, indarrean dagoen erreferentziazko legeriaren arabera, beharrezko segurtasun-neurriak hartuz ekiditeko datuok eraldatzea, galtzea edo baimena ez duten hirugarrengoek datuok tratatzea edo atzitzea.

Aldiz, erabiltzaileak zuzenean bere galdetegien bidez kudeatutako datuak pribatutasun-politika propioz arautuko dira, eta bere erantzukizunpean.

LiberaForms zure datu pertsonalak atzitzeko aukerak murrizten saiatzen da, eskura ditugun neurri teknikoak hartuz. Hala, prozesu gehienak norberak kudeatu ahal izango ditu, LiberaFormseko teknikariek zerbitzuak mantentzeko bakarrik atzitu dezaten zerbitzaria, eta ez datuak kudeatzeko, Erabiltzaile batek esplizituki eskatu ezean bere zerbitzuan eskua sartzeko.

### 2.1. Sartu gure webgunean
Gure webgunean sartzen direnen pribatutasuna garrantzitsua da guretzat. Ez dugu banakoen jarraipenik egiten. Gure webguneetan sartzen zarenean, erabiltzaile anonimo gisa:

* Ez dugu informazio pertsonalik biltzen.
* Ez diegu informaziorik partekatzen, bidaltzen edo saltzen hirugarrengoei.
* Ez dugu informaziorik esplotatzen banakoen joerak eta jarrerak identifikatzeko.
* Ez dugu informazioa monetizatzen.

### Cookieak
Ezinbestekoak diren cookie propioak baino ez ditugu erabiltzen.

www.liberaforms.org webgunean aldi baterako cookie teknikoak erabiltzen ditugu webgunea behar bezala ibili dadin. Ez da informazio pertsonalik gordetzen web-nabigatzaileko cookie horietan.

docs.liberaforms.org eta blog.liberaforms.org webguneetan ez dugu cookierik erabiltzen.

LiberaForms aplikazioaren instantzia publikoetan saio-cookie bakar bat erabiltzen dugu, erabiltzaileak instantzia horietako batean izena ematean sortzen dena. Erabiltzaile-kontua eta lotutako zerbitzuak identifikatzeko erabiltzen da. Cookie hori aldi baterakoa da eta saioan jarraitu bitartean baino ez da mantentzen. Saioa edo nabigatzailea ixten baduzu edo gailua itzaltzen baduzu cookiea galdu egiten da.

### Web-analisia
[Plausible Analytics](https://plausible.io) erabiltzen dugu https://plausible.io/gunos webgunean erabilera-datu anonimoak bildu eta estatistikak egiteko. Horren helburua webguneko trafikoaren joera orokorren berri izatea da, eta ez banakoen jarraipena egitea. Datu guztiak agregatuta daude. Ez da datu pertsonalik gordetzen.

Datu hauek biltzen dira, besteak beste: erreferentziazko iturburuak, web-orri nagusiak, bisitaren iraupena eta webgunean sartzeko erabilitako gailuen informazioa (gailu mota, sistema eragilea, herrialdea eta nabigatzailea). Xehetasun guztiak [Plausible Analytics-en datuen politikan](https://plausible.io/data-policy) irakurri ditzakezu.

### 2.2. Informazio-buletinean izena ematea

Informazioa posta elektronikoz jasotzea hautatzen dutenentzat [ListMonk](https://listmonk.app) instantzia propio bat erabiltzen dugu berriok zuzenean haien postontzira bidaltzeko. [Maadix](https://maadix.net/es) posta elektroniko hornitzailearen bidez bidaltzen da posta elektroniko guztia.

Datu hauek baino ez dira eskatzen: izen bat, posta elektroniko helbide bat eta komunikaziorako hizkuntza.

Legezko oinarria erabiltzailearen baimena da. Edonoiz utzi dezakezu gure informazio-buletina, jasotako eposten oineko argibideei jarraituz.

### 2.3. LiberaFormsen zerbitzuan izena ematea

Gure jarraibidea argia da: behar dugun informazioa baino ez dugu jasoko eta zu izena eman duzun zerbitzuaz hornitzeko bakarrik prozesatuko dugu informazio hori.

LiberaFormsen instantziaren batean kontu bat sortzen baduzu, datu gutxi batzuk eskatuko dizkizugu: erabiltzailea, posta elektronikoa eta pasahitza.

Informazio horri esker zerbitzu-harremana mantendu, garatu eta kudeatu dezakegu, webgune honen bidez kontratatutako produktu edo zerbitzuetan. Helburu horrekin tratatutako datuak harremanak iraun bitartean gordeko dira eta, behin harremana bukatutakoan, legez aurreikusitako erantzukizun-euste eta -preskripzio epeetan.

Horrek, praktikan, hau dakar:

### Zer biltzen dugu eta zertarako erabiltzen dugu?

Posta elektroniko helbide bat behar da LiberaFormsen instantzia publikoetan kontu bat sortzeko. Helbide hori saioa has dezazun eta zerbitzua erabiltzeko baino ez da.

Datu guztiak oso-osorik Europan ostatatutako zerbitzarietan daude. Horri esker bermatzen da datu guztiak, alegia, zure datu pertsonalak zein zure galdetegi eta erantzunak, datuen pribatutasunaren gaineko Europar Batasuneko lege zorrotzez arautzen direla. Zure datuak ez dira inoiz EBtik ateratzen.

[Maadix](https://maadix.net/es) posta elektroniko hornitzailearen bidez bidaltzen da posta elektroniko guztia.

LiberaFormsek ez ditu atzituko Erabiltzaileak gure zerbitzarian gordetako datuak, erabiltzaileak esku hartzeko eskatu ezean.

Horren legezko oinarria Erabiltzaileari zerbitzua emateko hitzarmena betetzea da. Edonoiz ezabatu dezakezu zure LiberaForms kontua.

### Pribatutasun-laguntzailea erabiltzea
Hala hautatuta, norberak gure laguntzailea erabiltzean, informazio gehiago behar da, erregistratutako erabiltzaileak ematen duena tresna hau behar bezala ibil dadin.

Pribatutasun-laguntzailea aktibo badago, erregistratutako erabiltzailearen pribatutasun-arduradunarekin harremanetan jartzeko kanal bat eskatzen da galdetegia sortzean (norberarena izan daitekeen eposta helbide bat), eta baita ere aipaturiko galdetegia arautzen duen pribatutasun-politika duen URL bat, azken hori hautaz. Datu horiek erabiltzaile anonimoak artatzeko erabiltzen dira, galdetegiari erantzuten dioten bitartean edo ondoren.

### Zerbitzuaren komunikazioak

Erabiltzailea jakinaren gainean mantentzeko helburuz, zerbitzuaren informazioa edo ezinbesteko beste informazioren bat bidali ahal dizugu epostara.

[ListMonk](https://listmonk.app) instantzia propio bat erabiltzen dugu berriok zuzenean zure postontzira bidaltzeko. [Maadix](https://maadix.net/es) posta elektroniko hornitzailearen bidez bidaltzen da posta elektroniko guztia.

Horren legezko oinarria zerbitzua baldintza onenetan eman ahal izateko interes legitimoa da, eta horretarako erabiltzaileen datu pertsonalak tratatu behar ditugu. Ez da informazio-buletin bat, baizik eta zerbitzuarekin lotutako komunikazioak, eta beharrezkoak dira zerbitzua behar bezala emateko.

### Bestelako kontuak

Galdetegiak dokumentu publikoak dira. Galdetegia sortzean edonolako informazio pertsonal gehitzen baduzu (helbideak, telefono-zenbakiak...), izan zurea edo hirugarrengoena, ziurtatu dagokion baimena edo legitimitatea duzula. Nolanahi ere, zure erantzukizunpean izango da eta ez LiberaFormsenean.

### 2.4 Datuak gordetzea

Irizpide hauek erabiltzen ditugu Erabiltzailearen datu pertsonalei eusteko epea zehazteko:

• Galdetegien erantzunetan egon daitezkeen datu pertsonalak mugagabeki gordeko dira, erabiltzaileak bere kontua edo erantzunok ezabatu ezean.

• Baimen bidez lortutako datu pertsonalak: baimena kendu arte.

• Kontratu-harremanean jasotako edo sortutako datu pertsonalak: kontratuak abian iraun bitartean.

• Zalantzaren bat argitzeko gurekin harremanetan jartzean jasotako datu pertsonalak: zalantza argitu arte.

• Izena, telefono-zenbakia, helbidea eta eposta (harremanetarako datu pertsonalak): mugagabeki Erabiltzaileak datuok ezabatzea eskatu ezean.

• Gure betebehar legalak edo arauzkoak betetzeko behar ditugun datu pertsonalak, gure eskubideak kudeatzekoak (adibidez, epaietan gure erreklamazioak defendatzeko), edo estatistika edo erregistrotarako datu pertsonalak: gure betebeharrak, erreklamazioak eta abar betetzeko behar dugun denboran.

Erabiltzailearen datu pertsonalak gehiago erabili behar ez ditugunean, gure artxiboetatik ezabatuko ditugu. Kontuan izan 3 hilabeteko tartea dagoela jada egindako segurtasun kopietako datuak betirako ezabatzeko.

### 2.5 Datuak lagatzea

Ez da daturik lagatzen LiberaFormsen ingurunetik kanpo, ez zure daturik, ez zure galdetegietakorik eta erantzunik ere.

### 2.6 Nazioarteko transferentziak

Ez da daturik transferitzen LiberaFormsen ingurunetik kanpo, ez zure daturik, ez zure galdetegietakorik eta erantzunik ere.

### 2.7 Zure eskubideak

* Gure webgunean edo instantzia publikoetan zure daturik eman baduzu, eskatu egin ahal diguzu gordeta ditugun zure gaineko datuak biltzen dituen fitxategi bat bidaltzeko. Era berean, zure kontua ezabatu dezakezu edo zure edozein datu pertsonal ezabatzeko eskatu. Horrek ez du barne hartzen administrazio-, lege- edo segurtasun-arrazoiengatik gorde behar dugun daturik.
* Erabiltzaileak edonoiz erabili ahal izango ditu atzitze-, zuzentze-, indargabetze-, aurkaritza-, mugatze- edo lekualdatze-eskubideak, [info@liberaforms.org](mailto:info@liberaforms.org) eposta helbidera idatziz.
* Datuak tratatzeko legitimitatea baimenean oinarritzen denean, Erabiltzaileak edonoiz kendu dezake baimen hori.
* Datu pertsonalik urratuz gero, urratzearen berri dugunetik gehienez ere 72 orduko epean jakinaraziko diogu dagokion ikuskapen-autoritateari datuak urratu direla, ez bada inprobablea dela datuen urratzeak pertsona fisikoen eskubide eta askatasunentzako arriskurik ekartzea.
   Gogorarazten dizugu, nolanahi ere, eskubidea duzula erreklamazio bat aurkezteko zure tokiko agintaritzan, datuen babesari dagokion horretan.

## 3. Aldaketak eta zalantzak argitzea

Behar dugunean, politika hau eguneratu dezakegu dagozkion arauak betetzeko eta dena delako praktika berri islatzeko. Gure politikan aldaketa nabarmenik egiten dugun aldiro, [proiektuko blog](https://blog.liberaforms.org)ean edo sare sozialetan ditugun kontuetan ere emango dugu horren berri.

Pribatutasun-politika honi buruz galderarik, iruzkinik edo zalantzarik baduzu zure datuez edo zure informazioaren gainean dituzun eskubideez, jarri gurekin harremanetan [info@liberaforms.org](mailto:info@liberaforms.org) epostara idatziz.

_Azken aldaketa: 2023ko abuztuaren 30a._