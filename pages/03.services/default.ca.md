---
title: Serveis
media_order: 'liberafoms-modalitats-en.png,liberafoms-modalitats-es.png,liberafoms-modalitats-eu.png,liberafoms-modalitats-eo.png,liberafoms-modalitats-ca.png,liberafoms-modalitats-eu.svg,liberafoms-modalitats-en.svg,liberafoms-modalitats-es.svg,liberafoms-modalitats-eo.svg,liberafoms-modalitats-ca.svg,volume.en.png,basic.en.png,regular.en.png,basic.ca.png,regular.ca.png,casual.ca.png,volume.ca.png,casual.en.png,regular.eus.png,basic.eus.png,casual.es.png,casual.eus.png,volume.eus.png,basic.es.png,regular.es.png,volume.es.png,volume.ca2.png,basic2.ca.png,casual2.ca.png,regular2.ca.png,volume2.ca.png,regular2.ca.png,volume2.ca.png'
published: true
slug: serveis
---

# Serveis
Els serveis que oferim es divideixen en quatre plans.

<div class="service-images">
<img src="/user/pages/03.services/basic2.ca.png" />
<img src="/user/pages/03.services/casual2.ca.png" />
<img src="/user/pages/03.services/regular2.ca.png" />
<img src="/user/pages/03.services/volume2.ca.png" />
</div>

Els preus no inclouen impostos.

* Bàsic, fins a 250 respostes a l'any - Gratuït
* Casual, fins a 9.600 respostes a l'any - 90€/any
* Regular, fins a 36.000 respostes a l'any - 130€/any
* Gran volum, poseu-vos en contacte amb nosaltres per obtenir més de 36.000 respostes a l'any

Addicionalment, en els plans de pagament també podeu configurar [el vostre propi domini](#your-domain-name).

I no us preocupeu per escalar el vostre pla, sabem que no sempre podem preveure el volum esperat de respostes, per tant som plenament flexibles. Si puntualment supereu els volums fixats, tindreu flexibilitat. Si aquest pic persisteix en el temps, us contactarem abans de prendre cap decisió al respecte i, normalment, us demanarem que sospeseu fer una donació via [OpenCollective](https://opencollective.com/liberaforms) ;)

Si aquests plans no s'ajusten a les vostres necessitats, poseu-vos en contacte amb nosaltres a info [@] liberaforms.org i digueu-nos què necessiteu perquè puguem arribar a un acord.


## Característiques

Tots els plans tenen les mateixes característiques:

* Crear, estilitzar i publicar formularis en línia
* Consultar, editar i descarregar les respostes als vostres formularis mitjançant taules, PDF, gràfics, CSV i JSON
* Definir la pantalla de 'Gràcies!', que apareix un cop s'omple un formulari
* Rebre notificacions per correu electrònic
* Gestionar les declaracions de privacitat
* Incloure una casella per enviar un justificant de recepció
* Definir les condicions d'expiració (data, màxim de respostes, camps numèrics)
* Definir la pantalla de 'Aquest formulari ha caducat'
* Formes de col·laboració. Comparteix els teus formularis amb els teus companys i crea fluxos de treball
* Obteniu el codi HTML per inserir el vostre formulari en un altre lloc web.
* Consultar les teves estadístiques d'ús

I molt més!

## Crea un compte

Els nostres serveis es proporcionen en una de les nostres instàncies públiques com a SaaS (al núvol). Totes les instàncies ofereixen les mateixes característiques i només es diferencien pel seu nom de domini. Els vostres formularis es publicaran sota el domini de la instància, així que trieu la instància de quin domini us convingui més.

Creeu un compte en una de les nostres [instàncies públiques](https://www.liberaforms.org/ca#instancia) de manera gratuïta.

<h2 id="your-domain-name">El teu domini, el teu LiberaForms</h2>
Recomanat quan ets una organització gran o petita, o tens un gran volum de respostes.

Un servidor de LiberaForms amb el vostre nom de domini i reservat només per al vostre ús.

* Gestionar la configuració de l'administrador
* Personalitzar l'estil del servidor, idiomes, textos, polítiques, etc.
* Crear i gestionar múltiples usuaris
* Definiu Termes i condicions i Declaracions de privadesa
* Comparteix declaracions de privadesa amb altres usuaris del teu lloc
* Rebre notificacions d'activitats
* Els formularis es publiquen amb el vostre domini, per exemple: https<nolink>://forms.your-domain.com
     
Abans de contractar aquest servei, necessitareu un domini i un servidor de correu SMTP.

<!-- Entenem que algunes organitzacions no tenen un domini ni un servidor SMTP. Si no ho fas, potser et podem ajudar. -->

El teu propi LiberaForms costa **150€/any** i inclou:

* La instal·lació i configuració inicials
* Actualitzacions a noves versions de LiberaForms
* Servei de manteniment
* Còpies de seguretat
* Suport



