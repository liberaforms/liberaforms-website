---
title: Zerbitzuak
published: true
slug: zerbitzuak
media_order: 'basic.ca.png,basic.en.png,basic.es.png,casual.ca.png,casual.en.png,casual.es.png,liberafoms-modalitats-ca.png,liberafoms-modalitats-ca.svg,liberafoms-modalitats-en.png,liberafoms-modalitats-en.svg,liberafoms-modalitats-eo.png,liberafoms-modalitats-eo.svg,liberafoms-modalitats-es.png,liberafoms-modalitats-es.svg,liberafoms-modalitats-eu.png,liberafoms-modalitats-eu.svg,regular.ca.png,regular.en.png,regular.es.png,volume.ca.png,volume.en.png,volume.es.png,basic.eus.png,casual.eus.png,regular.eus.png,volume.eus.png'
---

# Zerbitzuak

Lau zerbitzu-modalitate ditugu.

<div class="service-images">
<img src="/user/pages/03.services/basic.eus.png" />
<img src="/user/pages/03.services/casual.eus.png" />
<img src="/user/pages/03.services/regular.eus.png" />
<img src="/user/pages/03.services/volume.eus.png" />
</div>

Prezioek ez dituzte zergak barne hartzen.

* Oinarrizkoa, urteko 250 erantzuneko mugarekin - Doan
* Noizbehinkakoa, urteko 9.600 erantzuneko mugarekin - 90 € urtean
* Ohikoa, urteko 36.000 erantzuneko mugarekin - 130 € urtean
* Bolumen handikoa. Jarri gurekin harremanetan urteko 36.000 erantzun baino gehiagorako

Modalitate guztiak konfiguratu daitezke [zeure domeinu pean](#zeure-domeinu-izena).

Modalitate horiek ez badira zeure beharretara egokitzen, idatzi iezaguzu [@] liberaforms.org eposta helbidera eta kontatu zer behar duzun, akordio batera iritsi gaitezen. batera

## Ezaugarriak

Modalitate guztietan ezaugarri berak izango dituzu:

* Internet bidezko galdetegiak sortu, itxuraldatu eta argitaratzea
* Zure galdetegietako erantzunak ikusi, editatu eta deskargatzea, tauletan, grafikoetan edo PDF, CSV edo JSON gisa
* Galdetegiari erantzun ondoren agertzen den Eskertze orria editatzea
* Jakinarazpenak eposta bidez jasotzea
* Datuen inguruko pribatutasun-adierazpenak kudeatzeq
* Kontrol-lauki bat gehitzea, erantzunak jaso direla berresten duen eposta mezu bat bidaltzeko
* Iraungitze-baldintzak zehaztea (data, zenbaki-eremuak)
* "Galdetegi hau iraungi egin da" orria zehaztea
* Galdetegiak elkarlanean kudeatzea Galdetegiak lankideekin partekatzea zuen erara lan egiteko
* Galdetegi beste webgune batean kapsulatzeko HTML kodea eskuratzea
* Eta askoz ere gehiago!

## Sortu kontu bat

SaaS gisa eskaintzen dugu zerbitzua gure instantzia publikoetan. Instantzia guztietan ezaugarri berdinak topatuko dituzu, domeinuaren izena baino ez da aldatzen. Zure galdetegiak hautatzen duzun instantziaren domeinu pean argitaratuko dira, hortaz, instantzia hautatzerakoan, begiratu zein domeinu komeni zaizun gehien.

Sortu kontua doan gure [instantzia publiko](https://www.liberaforms.org/eu#instancia)etako batean.

<h2 id="zeure-domeinu-izena">Zeure domeinua, zeure LiberaForms</h2>

Erakunde handi zein txikientzat gomendagarria, edo erantzun-bolumen handia baldin baduzu.

Zure domeinu izena duen LiberaForms zerbitzari bat, zeuk bakarrik erabiltzekoa.

* Kudeatu Administratzaileen ezarpenak
* Pertsonalizatu zerbitzariaren itxura, hizkuntzak, testuak, politikak eta abar
* Nahi adina erabiltzaile sortu eta kudeatu
* Erabilera-baldintzak eta pribatutasun-adierazpenak kudeatu, DBEO politika esaterako
* Pribatutasun-adierazpenak zure webguneko erabiltzaileekin partekatu
* Jardueren jakinarazpenak jaso
* Galdetegiak zure domeinua erabiliz argitaratzen dira, adibiderako, https<nolink>://galdetegiak.zure-domeinua.eus

Zerbitzu hau kontratatu aurretik, domeinu bat eta SMTP eposta zerbitzari bat beharko dituzu.

<!-- We understand that some organizations do not have a domain or an SMTP server. If you don't, we may be able to help. -->

Zeure LiberaForms propioa izateak urtean 150 € balio du eta hau guztia dakar:

* Hasierako instalazioa eta konfigurazioa
* LiberaFormsen bertsio berrietarako eguneratzeak
* Zerbitzariaren mantenua
* Babeskopiak
* Laguntza
