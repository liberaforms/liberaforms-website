---
title: Services
published: true
slug: services
media_order: 'liberafoms-modalitats-en.png,liberafoms-modalitats-es.png,liberafoms-modalitats-eu.png,liberafoms-modalitats-eo.png,liberafoms-modalitats-ca.png,liberafoms-modalitats-eu.svg,liberafoms-modalitats-en.svg,liberafoms-modalitats-es.svg,liberafoms-modalitats-eo.svg,casual.en.png,liberafoms-modalitats-ca.svg,basic.en.png,regular.en.png,volume.en.png'
---

# Services

The services we offer are divided into four plans.

<div class="service-images">
<img src="/user/pages/03.services/basic.en.png" />
<img src="/user/pages/03.services/casual.en.png" />
<img src="/user/pages/03.services/regular.en.png" />
<img src="/user/pages/03.services/volume.en.png" />
</div>

Prices do not include tax.

* Basic, up to 250 replies per year - Free of charge
* Casual, up to 9.600 replies per year - 90€/year
* Regular, up to 36.000 replies per year - 130€/year
* High volume, contact us for more than 36.000 replies per year

Each of these plans can be set up under [your own domain](#your-domain-name).

If these plans do not fit your needs, please contact us at info [@] liberaforms.org and tell us what you need so we can come to an agreement.

## Features

All plans come with the same features:

* Create, style and publish online forms
* Consult, edit and download the replies to your forms via tables, PDF, graphs, CSV and JSON
* Edit the 'Thank you' page that appears after the form has been submitted
* Receive notifications via email
* Manage Data privacy statements
* Include a checkbox to send an acknowledgement of receipt email
* Define expiry conditions (date, numeric fields)
* Define the 'This form has expired' page.
* Form collaboration. Share your forms with colleagues and create workflows
* Get the HTML code to embed your form in another website.
* And much more!

## Create an account

Our services are provided at one of our public instances as a SaaS. All instances offer the same features and differ only in their domain name. Your forms will be published under the domain of the instance so choose the instance who's domain best suits you.

Create an account on one of our [public instances](https://www.liberaforms.org/en#instancia) for free.

<h2 id="your-domain-name">Your domain, your LiberaForms</h2>

Recommended when you are an organization big or small, or have a high volume of answers.

A LiberaForms server with your domain name and reserved for your use only.

* Manage the Administrator's settings
* Personalize the server's style, languages, texts, policies, etc.
* Create and manage multiple users
* Define Terms and conditions, and privacy statements like the GDPR policy
* Share privacy statements with the users on your site
* Receive activity notifications
* Forms are published using your domain, for example: https<nolink>://forms.your-domain.com

Before contracting this service, you will need a domain and an SMTP email server.

<!-- We understand that some organizations do not have a domain or an SMTP server. If you don't, we may be able to help. -->

Your own LiberaForms costs 150€/year and includes:

* The initial installation and setup
* Upgrades to new versions of LiberaForms
* Server maintenance
* Backup copies
* Support


