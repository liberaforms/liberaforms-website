---
title: Servicios
published: true
slug: servicios
media_order: 'liberafoms-modalitats-en.png,liberafoms-modalitats-es.png,liberafoms-modalitats-eu.png,liberafoms-modalitats-eo.png,liberafoms-modalitats-ca.png,liberafoms-modalitats-eu.svg,liberafoms-modalitats-en.svg,liberafoms-modalitats-es.svg,liberafoms-modalitats-eo.svg,liberafoms-modalitats-ca.svg,volume.en.png,basic.en.png,regular.en.png,basic.ca.png,regular.ca.png,casual.ca.png,volume.ca.png,casual.en.png,basic.es.png,casual.es.png,regular.es.png,volume.es.png'
---

# Servicios
Los servicios que ofrecemos se dividen en cuatro planes.

<div class="service-images">
<img src="/user/pages/03.services/basic.es.png" />
<img src="/user/pages/03.services/casual.es.png" />
<img src="/user/pages/03.services/regular.es.png" />
<img src="/user/pages/03.services/volume.es.png" />
</div>

Los precios no incluyen impuestos.

* Básico, hasta 250 respuestas al año - Gratuito
* Casual, hasta 9.600 respuestas al año - 90€/año
* Regular, hasta 36.000 respuestas al año - 130€/año
* Gran volumen, ponte en contacto con nosotros para obtener más de 36.000 respuestas al año

Cada uno de estos planes se puede configurar en [tu propio dominio](#your-domain-name).

Si estos planes no se ajustan a tus necesidades, ponte en contacto con nosotros en info [@] liberaforms.org y dinos qué necesitas para que podamos llegar a un acuerdo.

## Características

Todos los planes tienen las mismas características:

* Crear, estilizar y publicar formularios online
* Consultar, editar y descargar las respuestas a tus formularios mediante tablas, PDF, gráficos, CSV y JSON
* Definir la pantalla de 'Gracias!', que aparece una vez se rellena un formulario
* Recibir notificaciones por correo electrónico
* Gestionar las declaraciones de privacidad
* Incluir una casilla para enviar un acuse de recibo
* Definir las condiciones de expiración (fecha, máximo de respuestas, campos numéricos)
* Definir la pantalla de 'Este formulario ha caducado'
* Formas de colaboración. Comparte tus formularios con tus compañeros y crea flujos de trabajo
* Obten el código HTML para insertar tu formulario en otro sitio web.
* Consultar tus estadísticas de uso

¡Y mucho más!

## Crear una cuenta

Nuestros servicios se proporcionan en una de nuestras instancias públicas como SaaS (en la nube). Todas las instancias ofrecen las mismas características y sólo se diferencian por su nombre de dominio. Tus formularios se publicarán bajo el dominio de la instancia, así que elige la instancia con el dominio que te convenga más.

Crea una cuenta en una de nuestras [instancias públicas](https://www.liberaforms.org/es#instancia) de forma gratuita.

<h2 id="your-domain-name">Tu dominio, tu LiberaForms</h2>
Recomendado cuando eres una organización grande o pequeña o tienes un gran volumen de respuestas.

Un servidor de LiberaForms con tu nombre de dominio y reservado sólo para tu uso.

* Gestiona la configuración del administrador
* Personaliza el estilo del servidor, idiomas, textos, políticas, etc.
* Crea y gestiona múltiples usuarios
* Define Términos y condiciones y Declaraciones de privacidad
* Comparte declaraciones de privacidad con otros usuarios de tu sitio
* Recibe notificaciones de actividades
* Los formularios se publican bajo tu dominio, por ejemplo: https<nolink>://forms.your-domain.com
     
Antes de contratar este servicio, necesitarás un dominio y un servidor de correo SMTP.

<!-- Entendemos que algunas organizaciones no tienen un dominio ni un servidor SMTP. Si no lo haces, quizás podemos ayudarte. -->

Tu propio LiberaForms cuesta **150€/año** e incluye:

* La instalación y configuración iniciales
* Actualizaciones a nuevas versiones de LiberaForms
* Servicio de mantenimiento
* Copias de seguridad
* Soporte
    
    
    