---
title: Butlletí
slug: butlleti
metadata:
    keywords: 'butlletí, liberaforms, email, libre, forms'
    description: 'Rep les novetats de LiberaForms directament a la teva bústia'
published: true
visible: false
---

# Butlletí electrònic
Rep les novetats sobre LiberaForms a la teva bústia de correu, uns pocs cops a l'any.

## Formulari de subscripció
Completa les teves dades de contacte i rebràs un correu amb la invitació al nostre butlletí electrònic.

<form method="post" action="https://lists.liberaforms.org/subscription/form" class="listmonk-form">
    <div>
        <input type="hidden" name="nonce" />
        <p><input type="email" name="email" required placeholder="Correu electrònic" /></p>
        <p><input type="text" name="name" placeholder="Nom (opcional)" /></p>
      
        <p>
          <input id="e64af" type="checkbox" name="l" value="e64af37b-6a18-4af8-a2b8-b92d006c9c7d" />
          <label for="e64af"><b>Butlletí informatiu de LiberaForms</b></label>
           <p style="font-size:0.8em">Les teves dades personals seran tractades segons els criteris que fixa aquesta declaració de privacitat:<br>
- PROPÒSIT: mantenir comunicació informativa via butlletí periòdic.<br>
- LEGITIMITAT: per consentiment, que pots revocar en qualsevol moment.<br>
- RESPONSABLE: Estraperlo S.Coop. de C-LM. Correu electrònic: <a href="mailto:info@liberaforms.org">info@liberaforms.org</a>. Web site: <a href="https://liberaforms.org/en/privacy-policy" target="_blank">https://liberaforms.org/ca/politica-privacitat</a><br>
- Segons el Reglament General de Protecció de Dades (EU) 2016/679.<br>
Si tens qualsevol dubte, adreçat al contacte que aquí s'indica.</p>
        <p><input type="submit" value="Subscriu-te" /></p>
    </div>
</form>