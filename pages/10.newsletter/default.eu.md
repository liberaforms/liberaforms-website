---
title: Buletina
slug: buletina
metadata:
    keywords: 'buletina, liberaforms, posta elektronikoa, libre, formularioak'
    description: 'Jaso LiberaForms-en albisteak zuzenean zure postontzian'
published: true
visible: false
---

# Buletina
Jaso noizean behin LiberaForms-i buruzko berriak epostontzian bertan.

## Harpidetzeko galdetegia
Osatu galdetegia zure harremanetarako informazioarekin eta gure buletin elektronikorako gonbidapen mezu bat jasoko duzu epostaz.

<form method="post" action="https://lists.liberaforms.org/subscription/form" class="listmonk-form">
    <div>
        <input type="hidden" name="nonce" />
        <p><input type="email" name="email" required placeholder="Eposta" /></p>
        <p><input type="text" name="name" placeholder="Izena (hautazkoa)" /></p>

        <p>
          <input id="db85f" type="checkbox" name="l" value="db85f0e1-dcf8-402a-bfce-9c8faf1fa681" />
          <label for="db85f">LiberaForms-en buletina</label>
    		<p style="font-size:0.8em">Zure datu pertsonalak pribatutasun-adierazpen honen irizpideen arabera prozesatuko dira:<br>
    		- HELBURUA: informazio-komunikazioa mantentzea aldizkako buletin baten bidez.<br>
    		- LEGITIMITATEA: zeuk baimendua, edozein unetan bertan behera utzi dezakezuna.<br>
    		- ARDURADUNA: Estraperlo S.Coop. C-LM-koa. Eposta: <a href="mailto:info@liberaforms.org">info@liberaforms.org</a>. Webgunea: <a href="https://liberaforms.org/eu/pribatutasun-politika" target="_blank">https://liberaforms.org/eu/pribatutasun-politika</a><br>
    		- Datuak Babesteko Erregelamendu Orokorra (EU) 2016/679 arauaren arabera.<br>
    		Zalantzarik izanez gero, idatzi lehen adierazitako kontaktura.</p>
        <p><input type="submit" value="Harpidetu" /></p>
    </div>
    </form>