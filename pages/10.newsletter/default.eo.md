---
title: Bulteno
slug: bulteno
metadata:
    keywords: 'bulteno, liberaforms, email, libre, formoj'
    description: 'Ricevu novaĵojn pri LiberaForms, rekte en via leterkesto.'
published: true
visible: false
---

# Bulteno
Ricevu novaĵojn pri LiberaForms en via leterkesto.

## Abonformularo
Plenigu ciajn kontaktinformojn kaj ci ricevos retpoŝton kun la invito al nia reta bulteno.

<form method="post" action="https://lists.liberaforms.org/subscription/form" class="listmonk-form">
    <div>
        <input type="hidden" name="nonce" />
        <p><input type="email" name="email" required placeholder="Retpoŝto" /></p>
        <p><input type="text" name="name" placeholder="Nomo (nedeviga)" /></p>
      
        <p>
          <input id="49053" type="checkbox" name="l" value="4905306c-d26c-41e0-8f1f-efdebd6827d5" />
          <label for="49053">Bulteno</label><br><br>
          Abonante, mi akceptas la <a href="https://liberaforms.org/eo/privatan-politikon" target="_blank">privatecan politikon</a>.</p>
         <p><input type="submit" value="Aboni" /></p>
    </div>
</form>