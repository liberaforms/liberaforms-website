---
title: Newsletter
slug: newsletter
metadata:
    keywords: 'newsletter, liberaforms, email, libre, forms'
    description: 'Receive news about LiberaForms, directly in your mailbox.'
published: true
visible: false
---

# Newsletter
Receive occasional news about LiberaForms directly in your mailbox.

## Subscription form
Fill out your contact information and you will receive an email with the invitation to our electronic newsletter.

<form method="post" action="https://lists.liberaforms.org/subscription/form" class="listmonk-form">
    <div>
        <input type="hidden" name="nonce" />
        <p><input type="email" name="email" required placeholder="Email" /></p>
        <p><input type="text" name="name" placeholder="Name (optional)" /></p>
      
        <p>
          <input id="db85f" type="checkbox" name="l" value="db85f0e1-dcf8-402a-bfce-9c8faf1fa681" />
          <label for="db85f">LiberaForms Newsletter</label>
			<p style="font-size:0.8em">Your personal data will be processed according to the following privacy statement criteria:<br>
			- PURPOSE: maintain informative communication via regular newsletter.<br>
			- LEGITIMACY: by consent, which you can cancel at any time.<br>
			- DATA CONTROLLER: Estraperlo S.Coop. of C-LM. Email: <a href="mailto:info@liberaforms.org">info@liberaforms.org</a>. Website: <a href="https://liberaforms.org/en/privacy-policy" target="_blank">https://liberaforms.org/en/privacy-policy</a><br>
			- According to the General Data Protection Regulation (EU) 2016/679.<br>
			If you have any questions, address them to the contact stated above.</p>
        <p><input type="submit" value="Subscribe" /></p>
    </div>
</form>