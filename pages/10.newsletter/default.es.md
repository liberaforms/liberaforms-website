---
title: Boletín
slug: boletin
metadata:
    keywords: 'boletín, liberaforms, email, libre, formularios'
    description: 'Recibe las novedades de LiberaForms directamente en tu buzón'
published: true
visible: false
---

# Boletín electrónico
Recibe novedades sobre LiberaForms en tu buzón de correo.

## Formulario de subscripción
Completa tus datos de contacto y recibirás un correo con la invitación a nuestro boletín electrónico.

<form method="post" action="https://lists.liberaforms.org/subscription/form" class="listmonk-form">
    <div>
        <input type="hidden" name="nonce" />
        <p><input type="email" name="email" required placeholder="Correo electrónico" /></p>
        <p><input type="text" name="name" placeholder="Nombre (opcional)" /></p>
      
        <p>
          <input id="f9d8e" type="checkbox" name="l" value="f9d8e9bb-a552-4c8f-900d-680ba9415d9e" />
          <label for="f9d8e">Boletín de LiberaForms</label>
              <p style="font-size:0.8em">Tus datos personales serán tratados según los criterios que fija esta declaración de privacidad:<br>
- PROPÓSITO: mantener comunicación informativa vía boletín periódico.<br>
- LEGITIMIDAD: por consentimiento, que puedes revocar en cualquier momento.<br>
- RESPONSABLE: Estraperlo S.Coop. de C-LM. Correo electrónico: <a href="mailto:info@liberaforms.org">info@liberaforms.org</a>. Web site: <a href="https://liberaforms.org/es/politica-privacidad" target="_blank">https://liberaforms.org/es/politica-privacidad</a><br>
- Según el Reglamento General de Protección de Datos (EU) 2016/679.<br>
Si tienes cualquier duda, dirígete al contacto que aquí se indica.</p>
              <p><input type="submit" value="Subscríbete" /></p>
    </div>
</form>