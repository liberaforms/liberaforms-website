---
title: Participa
published: false
slug: participa
---

# Participa
Hi ha moltes maneres de participar al projecte LiberaForms.

## Usa i difon
### Usa'l
Usar el programa és donar-li suport, i per tant, millorar-lo. Recolza'ns tot creant i compartint formularis amb LiberaForms!

### Parla'n
Difondre el projecte també és participar! Parla'n a persones, col·lectius i entitats perquè coneguin el projecte. 

### Blogs
Si tens un blog i ens vols dedicar unes línies, ens agradarà llegir sobre les teves impressions. Informa'ns que has escrit una entrada per la Fediverse o per correu electrònic. 

### Mitjans socials
Si difons per mitjans socials, pots ajudar-nos a fer pinya usant l'etiqueta #LiberaForms. I si ens vols mencionar a la Fediverse, som al node barcelona.social.</br> 
[LiberaForms a la Fedi](https://mastodont.cat/@LiberaForms)

## Millora el projecte
### Codi
Si ets una persona familiaritzada amb el desenvolupament de programari i trobes algun error al codi o vols proposar una millora, t'agraïrem que ens ho facis saber a través de la forja.</br> 
[Forja LiberaForms](https://gitlab.com/liberaforms)

### Documentació
Si t'adones d'alguna errada a la documentació o vols proposar millores, t'agraïrem que ens ho facis saber a través de la Fedi o per correu electrònic.</br> 
[Documentació LiberaForms](https://docs.liberaforms.org/)

### Traduccions
Si vols que LiberaForms estigui en el teu idioma, dona’ns un cop de mà amb les traduccions a través de Weblate.</br> 
[Traduccions LiberaForms](https://docs.liberaforms.org/ca/participate/L10n/)

## Implica't
Implicar-se al projecte vol dir contribuir al seu desenvolupament, tant en la part de la infraestrura com en la dels continguts. Si vols implicar-te a LiberaForms, llegeix primer el Contracte Social i el Codi de Conducta.  

* [Contracte Social](https://docs.liberaforms.org/ca/participate/social-contract/)
* [Codi de Conducta](https://docs.liberaforms.org/ca/participate/code-of-conduct/)

## Finança'l
### Contracta un servei
Si després de provar el programa, creus que en faràs un ús més intensiu o vols personalitzar LiberaForms sota el teu domini, fes una ullada als serveis que proposem. I si no t'encaixen amb el que necessites, escriu-nos un correu per poder personalitzar el servei a mida.</br> 
[Serveis LiberaForms](https://liberaforms.org/ca/serveis)

### Fes una donació
També pots donar suport fent una donació puntual o recurrent. Les donacions a LiberaForms es gestionen a través d'OpenCollective.</br> 
[Fes una donació](https://opencollective.com/liberaforms)
