---
title: Inicio
published: true
slug: inicio
---

# Formularios éticos con LiberaForms

LiberaForms es una herramienta de software libre pensada y desarrollada como infraestructura comunitaria, libre y ética que permite crear y gestionar formularios que respetan los derechos digitales de las personas que hacen uso de ellos.

Con LiberaForms puedes consultar, editar y descargar las respuestas recibidas; incluir una casilla para pedir consentimiento sobre la Ley de Protección de Datos; colaborar con otras usuarias compartiendo permisos; y muchas cosas más! 

## Formularios de ejemplo

Mira estos formularios de ejemplo y descubre cómo puede verse LiberaForms:

<div class="showcase-forms">
<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form"><img src="/user/pages/01.home/bush-walk-150.png"></a>
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form"><img src="/user/pages/01.home/activity-feedback-150.png"></a>
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form"><img src="/user/pages/01.home/summer-course-150.png "></a>
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form"><img src="/user/pages/01.home/project-application-150.png"></a>
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form"><img src="/user/pages/01.home/night-of-music-150.png"></a>
</div>

<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form">La caminata por el bosque de este mes</a> Organiza una actividad para el fin de semana.<br>
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form">Comentarios sobre una actividad</a> Facilita tus comentarios sobre una actividad.<br>
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form">Cursos de verano de tecnología</a> Reserva tu plaza para un pròximo curso.<br>
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form">Inscripción de un proyecto</a> ¿Tienes un proyecto nuevo? Inscríbelo para recibir apoyo financiero.<br>
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form">Gana una noche increible</a> Deja tu nombre y opta a un premio especial.

Consulta la [Guía del usuario](https://docs.liberaforms.org/es/user-guide/getting-started/) (en inglés) para obtener más capturas de pantalla.

<a id="instancia"></a>
## Crea una cuenta

Crea una cuenta <u>**gratuita y limitada a 250 respuestas por año**</u> en una de nuestras instancias públicas.

¡Elige el dominio que más te convenga!

* Para una dirección web tipo **https://my.liberaforms.org/my-form**, hazte usuario [aquí](https://my.liberaforms.org/)
* Para una dirección web tipo **https://usem.liberaforms.org/mi-formulario**, hazte usuario [aquí](https://usem.liberaforms.org)
* Para una dirección web tipo **https://erabili.liberaforms.org/mi-formulario**, hazte usuario [aquí](https://erabili.liberaforms.org)

## Instala tu propio

LiberaForms es cultura libre bajo licencia AGPLv3. ¡Úsalo, compártelo y mejóralo!

* Si quieres instalar tu propio LiberaForms, [consulta el código](https://gitlab.com/liberaforms/liberaforms).
* Anunciamos [nuevas versiones del software](https://blog.liberaforms.org) y otras novedades en nuestro blog.