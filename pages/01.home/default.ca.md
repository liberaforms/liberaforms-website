---
title: Inici
published: true
slug: inici
media_order: 'Captura de pantalla de 2023-09-25 20-21-08.png,Captura de pantalla de 2023-09-25 20-21-18.png,Captura de pantalla de 2023-09-25 20-21-23.png,Captura de pantalla de 2023-09-25 20-21-27.png,Captura de pantalla de 2023-09-25 20-21-29.png'
---

# Formularis ètics amb LiberaForms

LiberaForms és una eina de programari lliure pensada i desenvolupada com a infraestructura comunitària, lliure i ètica que permet crear i gestionar formularis que respecten els drets digitals de les persones que en fan ús.

Amb LiberaForms pots consultar, editar i descarregar les respostes rebudes; incloure una casella per a demanar consentiment sobre la Llei de Protecció de Dades; col·laborar amb altres usuàries compartint permissos; i moltes coses més!

## Formularis d'exemple

Mira aquests formularis d'exemple i descobreix com es pot veure LiberaForms:

<div class="showcase-forms">
<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form"><img src="/user/pages/01.home/bush-walk-150.png"></a>    
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form"><img src="/user/pages/01.home/activity-feedback-150.png"></a>
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form"><img src="/user/pages/01.home/summer-course-150.png"></a>
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form"><img src="/user/pages/01.home/project-application-150.png "></a>
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form"><img src="/user/pages/01.home/night-of-music-150.png"></a>
</div>

<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form">La caminada pel bosc d'aquest mes</a> Organitzeu una activiatat de cap de setmana.<br>
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form">Comentaris sobre l'activitat</a> Feu arribar els vostres comentaris sobre una activitat.<br>
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form">Cursos d'estiu de tecnologia</a> Reserva la teva plaça per a un curs proper.<br>
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form">Inscripció d'un projecte</a> Tens un projecte nou? Inscriu-lo per rebre suport financer.<br>
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form">Guanyeu una nit de somnis!</a> Deixa el teu nom i opta a un premi especial.


Consulteu la [Guia de l'usuari](https://docs.liberaforms.org/ca/user-guide/getting-started/) (en anglès) per obtenir més captures de pantalla.

<a id="instancia"></a>
## Crea un compte

Creeu un compte <u>**gratuït i limitat a 250 respostes per any**</u> en una de les nostres instàncies públiques.

Trieu el domini que més us convingui!

* Per a una adreça web tipus **https://my.liberaforms.org/my-form**, fes-te usuàri [aquí](https://my.liberaforms.org)
* Per a una adreça web tipus **https://usem.liberaforms.org/el-meu-formulari**, fes-te usuàri [aquí](https://usem.liberaforms.org)
* Per a una adreça web tipus **https://erabili.liberaforms.org/el-meu-formulari**, fes-te usuàri [aquí](https://erabili.liberaforms.org)

## Instal·la el teu propi

LiberaForms és cultura lliure sota llicència AGPLv3: usa'l, comparteix-lo i millora'l!

* Si vols instal·lar una instància de LiberaForms, [consulta el codi](https://gitlab.com/liberaforms)
* Presentem [noves versions del programari](https://blog.liberaforms.org/category/releases/) i altres notícies al nostre blog.
