---
title: Hasiera
published: true
slug: hasiera
---

# LiberaForms, galdetegi etikoak egiteko softwarea

LiberaForms software libreko tresna bat da, azpiegitura komunitario, libre eta etiko gisa pentsatu eta garatua, erabiltzen duten pertsonen eskubide digitalak errespetatzen dituzten galdetegiak sortu eta kudeatzeko aukera ematen duena.

LiberaFormsen bidez zure galdetegietan jasotako erantzunak kontsultatu, editatu eta deskargatu ditzakezu; datu pertsonalak babesteko Legea onartzeko kontrol-lauki bat gehitu; beste erabiltzaile batzuekin elkarlanean aritu zaitezke, behin baimenak partekatuta; eta askoz ere gehiago!


## Adibidezko galdetegiak

Eman begiratu bat adibidezko galdetegi hauei, LiberaFormsek zer itxura duen ikusteko.

<div class="showcase-forms">
<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form"><img src="/user/pages/01.home/bush-walk-150.png"></a>
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form"><img src="/user/pages/01.home/activity-feedback-150.png"></a>
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form"><img  src="/user/pages/01.home/summer-course-150.png"></a>
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form"><img src="/user/pages/01.home/project-application-150.png"></a>
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form"><img  src="/user/pages/01.home/night-of-music-150.png"></a>
</div>

<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form">Hilabeteko mendi-buelta</a> Antolatu asteburuko ibilaldia.  
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form">Jarduera ondorengo iritzia</a> Eman jarduera baten ondorengo iritzia.  
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form">Udako teknologia ikastaroak</a> Gorde zure tokia ikastaroan.  
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form">Proiektuarentzako eskaera</a> Proiektu berri bat duzu? Eskatu diru-laguntza.  
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form">Hartu parte kontzertu baterako zozketan!</a> Eman izena eta zorte on!


Ikusi pantaila-argazki gehiago [Erabiltzaileentzako argibideetan](https://docs.liberaforms.org/user-guide/getting-started/).

<a id="instancia"></a>
## Sortu kontu bat

Sortu kontu bat <u>**doan eta urtean 250 erantzuneko mugarekin**</u> gure euskal komunitatearentzako instantzia publikoan: <https://erabili.liberaforms.org>

Bestela, hautatu gehien komeni zaizun domeinua!

* Zure galdetegiek **https://my.liberaforms.org/nire-galdetegia** moduko web helbidea izateko, sortu [hemen](https://my.liberaforms.org/user/new) zure kontua
* Zure galdetegiek **https://usem.liberaforms.org/nire-galdetegia** moduko web helbidea izateko, sortu [hemen](https://usem.liberaforms.org/user/new) zure kontua
* Zure galdetegiek **https://erabili.liberaforms.org/nire-galdetegia** moduko web helbidea izateko, sortu [hemen](https://erabili.liberaforms.org/user/new) zure kontua


## Instalatu zeurea

LiberaForms AGPLv3 lizentziapeko kultura librea da: erabili, partekatu, hobetu!

* Zeure LiberaForms propioa instalatu nahi baduzu, eman begiratu bat [kodeari](https://gitlab.com/liberaforms/liberaforms)
* Blogean ematen dugu [softwarearen bertsio berrien](https://blog.liberaforms.org/category/releases/) berri.



