---
title: Ĉefpaĝo
published: true
slug: cxefpagxo
---

# Etikaj formularoj kun LiberaForms
LiberaForms estas ilo de libera programaro pensita kaj programita kiel komunan infrastrukturon, liberan kaj etikan, kiu ebligas krei kaj administri formularojn, kiuj respektas la ciferecajn rajtojn de homoj, kiuj uzas ĝin.

Kun LiberaForms vi povas konsulti, modifi kaj elŝuti la respondojn ricevitajn; inkludi markobutonon por peti konsenton pri la Leĝo de Protektado de Datumoj; kunlabori kun aliaj uzantoj per komunaj permesoj; kaj multaj aferoj pli!

Konu pli [la projekton!](/projekto)

LiberaForms estas liberkulturo sub licenco AGPLv3. Uzu, konigu kaj plibonigu ĝin!

* Se vi volas munti LiberaForms aperon, [kontrolu la kodon](https://gitlab.com/liberaforms/liberaforms)
* Ni anoncas [novajn versiojn de la programaro](https://blog.liberaforms.org/category/releases/) kaj aliajn novaĵojn en nia blogo.