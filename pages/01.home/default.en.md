---
title: Home
published: true
slug: home
media_order: 'project-application-150.png,summer-course-150.png,night-of-music-150.png,bush-walk-150.png,activity-feedback-150.png'
---

# LiberaForms, ethical form software

LiberaForms is a libre software tool developed as community, free, and ethical infrastructure that makes it easy to create and manage forms that respect the digital rights of the people who use it.

With LiberaForms you can browse, edit, and download the answers to your forms; include a checkbox to require Data protection law consent, collaborate with other users by sharing permissions; and much more!


## Demo forms

Check out these demo forms to see what LiberaForms looks like.

<div class="showcase-forms">
<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form"><img src="/user/pages/01.home/bush-walk-150.png"></a>
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form"><img src="/user/pages/01.home/activity-feedback-150.png"></a>
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form"><img  src="/user/pages/01.home/summer-course-150.png"></a>
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form"><img src="/user/pages/01.home/project-application-150.png"></a>
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form"><img  src="/user/pages/01.home/night-of-music-150.png"></a>
</div>

<a href="https://my.liberaforms.org/this-months-bush-walk-demo-form" target="demo_form">This month's bush walk</a> Organize a weekend walk.  
<a href="https://my.liberaforms.org/activity-feedback-demo-form" target="demo_form">Activity Feedback</a> Give your feedback about an activity.  
<a href="https://my.liberaforms.org/summer-courses-demo-form" target="demo_form">Tech summer courses</a> Reserve your place for an upcoming course.  
<a href="https://my.liberaforms.org/project-application-demo-form" target="demo_form">Project Application</a> Got a new project? Apply for financial support.  
<a href="https://my.liberaforms.org/a-night-of-music-demo-form" target="demo_form">Win a night out!</a> Put your name on the list for a special prize.


See the [User guide](https://docs.liberaforms.org/user-guide/getting-started/) for more screen shots.

<a id="instancia"></a>
## Create an account

Create an account for <u>**free and limited to 250 answers a year**</u> on one of our public instances.

Choose the domain that suits you best!

* For a web address like **https://my.liberaforms.org/my-form**, create your account [here](https://my.liberaforms.org)
* For a web address like **https://usem.liberaforms.org/my-form**, create your account [here](https://usem.liberaforms.org)
* For a web address like **https://erabili.liberaforms.org/my-form**, create your account [here](https://erabili.liberaforms.org)


## Install your own

LiberaForms is Libre culture published under the AGPLv3 license: use it, share it, improve it!

* If you want to install your own LiberaForms, check out [the code](https://gitlab.com/liberaforms/liberaforms)
* We announce [new versions of the software](https://blog.liberaforms.org/category/releases/) on our blog.


<!--
<link rel="stylesheet" href="/user/themes/libera-forms/js/lightbox/glightbox.min.css" />
<script src="/user/themes/libera-forms/js/lightbox/glightbox.min.js"></script>
<script type="text/javascript">
  GLightbox({});
</script>
-->