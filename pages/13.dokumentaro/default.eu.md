---
title: Dokumentazioa
published: false
slug: dokumentazioa
---

# Dokumentazioa
Hemen LiberaForms proiekturako dokumentazioa azaltzen dugu.

## Erabilera gida
LiberaForms-ekin galdetegiak sortzen edo instantzia bat kudeatzen hasi nahi baduzu, eman begirada bat erabilera gidari.  
[LiberaForms-en erabilera gida](https://docs.liberaforms.org)


## Gobernantza
LiberaForms-en Kontratu soziala eta Jokabide-kodea adostu ditugu. Proiektuan sartu nahi baduzu, bi dokumentu hauek irakurri eta beren edukiarekin ados egon beharko duzu.

* [Kontratu soziala](https://docs.liberaforms.org/eu/participate/social-contract/)
* [Jokabide-kodea](https://docs.liberaforms.org/eu/participate/code-of-conduct/)

