---
title: Documentació
published: false
slug: documentacio
---

# Documentació
En aquest espai detallem la documentació per al projecte LiberaForms.

## Guia d'ús
Si vols començar a crear formularis amb LiberaForms o a administrar una instància, fes una ullada a la guia d'ús.  
[Guia d'ús de LiberaForms](https://docs.liberaforms.org)

## Governança
A LiberaForms hem acordat un Contracte social i un Codi de conducta. Si et vols implicar al projecte, hauràs de llegir i estar d'acords amb el contingut d'aquests dos documents.

* [Contracte Social](https://docs.liberaforms.org/ca/participate/social-contract/) 
* [Codi de Conducta](https://docs.liberaforms.org/ca/participate/code-of-conduct/)


