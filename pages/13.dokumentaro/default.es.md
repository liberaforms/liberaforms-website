---
title: Docs
published: false
slug: documentacion
---

# Documentación

En este espacio detallamos la documentación para el proyecto LiberaForms.

## Manual de uso
Si quieres empezar a crear formularios con LiberaForms o administrar una instancia, echa un ojo al manual de uso.  
[Manual de uso LiberaForms](https://docs.liberaforms.org)

## Governanza
En LiberaForms hemos acordado un Contrato Social y un Código de Conducta. Si te quieres implicar en el proyecto, tienes que leer y estar de acuerdo con el contenido de estos dos documentos.

* [Contrato Social](https://docs.liberaforms.org/es/participate/social-contract/) 
* [Código de Conducta](https://docs.liberaforms.org/es/participate/code-of-conduct/)

## Documentos de uso

+ [Polìtica de privacidad](/politica-de-privacidad)
+ [Términos de servicio](/terminos-del-servicio)
