---
title: Docs
published: false
slug: documentation
---

# Documentation
Here we list the different documentation available for the LiberaForms project.

## Users' guide
If you want to start creating forms with LiberaForms or administering an instance, take a look at the users' guide.  
[LiberaForms Users' guide](https://docs.liberaforms.org)

## Governance
At LiberaForms, we have agreed on a Social Contract and a Code of Conduct. If you want to get involved in the project, you will have to read and agree on the content of these two documents.

* [Social Contract](https://docs.liberaforms.org/participate/social-contract/)
* [Code of Conduct](https://docs.liberaforms.org/participate/code-of-conduct/)

