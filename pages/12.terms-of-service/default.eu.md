---
title: Erabilera-baldintzak
visible: false
slug: erabilera-baldintzak
---

# LiberaForms-en SaaS zerbitzuaren erabilera-baldintzak

Mila esker LiberaFormsen hodeiko zerbitzuan dagoen espresuki sortutako instantzia hau erabiltzeagatik!

## 1. Zerbitzuaren arduraduna identifikatzea

LiberaForms ekimen kolektibo bat da, software libreko komunitatearen laguntza duena internet bidezko galdetegi etikoak garatzeko.

Proiektuari buruz gehiago jakiteko sartu [https://liberaforms.org/eu/proiektua](https://www.liberaforms.org/es/proyecto) helbidean

LiberaFormsen hodeiko zerbitzu hau, Software as a Service (SaaS) izenez ezaguna, zeuk zeure instantzia instalatzearen alternatiba gisa eskaintzen da. Helburu bikoitza du: LiberaForms galdetegi etikoak egiteko tresna ezagutzera ematea eta LiberaForms proiektuaren kode irekia garatzeko kostuak ordaintzea.

Zerbitzu honen legezko ordezkaria Estraperlo S.Coop. Gaztela-Mantxakoa da, egoitza soziala Calle Matadero, 5 – 02.260 Fuentealbilla (Espainia) helbidean duena eta identifikazio fiskala F-02546489. Erakunde horrek liberaforms.org domeinupeko LiberaFormsen instantzia publikoen ordezkari gisa jarduten du.

Dokumentu honetan "proiektu", "gu", "gure", "zerbitzu" edo "zerbitzuak" esaten dugunean LiberaFormsez ari gara.

LiberaFormsen "instantzia publikoak" edo "espresuki sortutako instantziak" esatean LiberaFormsen SaaS zerbitzuaz ari gara.

## 2. Definizioak

### 2.1. Erabiltzailea

Erabiltzailea da LiberaFormsen webgune edo sistemaren batekin konektatzen den pertsona oro. LiberaFormsen bi erabiltzaile mota daude:

* Erregistratutako erabiltzaileak, gonbidatuak izan direnak, LiberaFormsen galdetegiak editatu eta sortu ditzaketenak eta erabiltzaile anonimoen datuak kontsultatu ditzaketenak.

* Erabiltzaile anonimoak, erregistratzeko beharrik gabe, erregistratutako erabiltzaileek LiberaFormsen sortutako galdetegiei erantzuten dietenak.

Horrez gain, LiberaForms taldeak beste bi rol hauek ditu:

* Administratzaileak, erregistratutako erabiltzaileen datuak zuzenean kudeatzen dituztenak, baina erabiltzaile anonimoen datuetara sarbiderik izan gabe.

* Sistema-administratzailea, zerbitzuak ondo funtzionatzeaz arduratzen dena eta, eskatuz gero, datu-basea atzitu dezakeena.

### 2.2. Zerbitzuak

LiberaFormsen SaaS zerbitzuak internet bidezko plataformetan zerbitzu digitalak eskaintzen ditu.

Bi zerbitzu mota daude: komunitate osoak eskuragarri dituen doako zerbitzuak eta ordainpeko zerbitzuak.

LiberaFormsek ahalik eta jende gehienari erraztu nahi dio doako zerbitzuak erabiltzea, haien datuen kontrola berreskuratu dezaketela jabetu daitezen laguntzeko. Hortaz, komunitate osoak eskura ditu [https://www.liberaforms.org/](https://www.liberaforms.org/es) webgunean zerrendatutako instantzia publiko guztiak, beren doako modalitatean. Modalitate horrek funtzionalitate guztiak ditu, baina urte natural bakoitzean jaso daiteken erantzun kopurua mugatzen du.

Horrez gain, ordainpeko zerbitzuak daude, [https://liberaforms.org/eu/zerbitzuak](https://www.liberaforms.org/es/servicios) web-orrian xeheki azaltzen direnak eta kontratatu ahal direnak. Zerbitzu horiek instantzia publiko horietan erregistratutako eta urteko erantzun kopuru handiagoa behar duten erabiltzaileentzat dira, edo haientzat espresuki sortutako instantzia bat behar duten erabiltzaileentzat.

### 2.3. Kuotak

Erabiltzaile batek LiberaFormsekin kontratatzen duen zerbitzuaren prezioa dira kuotak.

### 2.4. Laguntza-kanalak

LiberaFormsek zerbitzu multzo bat eskaintzen du erabiltzailearen esperientzia hobetzeko. Zerbitzu horiei laguntza-kanalak esaten diegu eta dokumentazio teknikoa eta erabiltzaile-dokumentazioa, erabiltzaileen arteko elkarlaguntza komunitarioa edo, banakako arretarako, posta elektronikoa dira.

## 3. Zerbitzuaren baldintzak

LiberaFormsek zerbitzua ematen die erabiltzaileei, beti ere horiek baldintza batzuk betetzen badituzte.

IRAKURRI ARRETAZ AKORDIO HAU ZERBITZUA ERABILI AURRETIK. ZERBITZUA ERABILTZEAN, ZEHAZTUTAKO BALDINTZAK BETEKO DITUZULA ONARTZEN DUZU

Hemen adierazten dira LiberaFormsen eta erabiltzaileen arteko zerbitzua arautzen dituzten baldintza juridiko, tekniko eta ekonomikoak.

Zerbitzuaren baldintza hauek LiberaFormsen SaaS zerbitzuko espresuki sortutako instantziei soilik egokitzen zaizkie, [https://www.liberaforms.org](https://www.liberaforms.org/es) helbidean argitaratutakoei. Ez dagozkie LiberaForms softwarea darabilten norberak ostatatutako instantziei, ez dituztenak Zerbitzuaren baldintzak behar euren zerbitzarietan ostatatuak daudelako.

Etorkizunean Zerbitzuaren baldintza hauek eguneratu ditzakegu. Gure politiketan aldaketa esanguratsu bat egiten dugun aldiro gure [blog](https://blog.liberaforms.org/)ean emango dugu aditzera.

Tarifa orokorrak [https://liberaforms.org/eu/zerbitzuak](https://www.liberaforms.org/es/servicios) web-orrian daude eskuragarri.

Erabiltzaileak eta LiberaFormsek unean-unean adostu ditzakete LiberaFormsen webgunean agertzen ez diren zerbitzu berezietarako tarifak.

Jarraian adierazitako baldintzak urratuz gero, zure kontua bertan behera geratu daiteke.

Hau adierazpen oso zabala da eta, horregatik, asko fidatu behar duzu gurekin. Gure eskuetan dagoen guztia egiten dugu gurekin fidatu zaitezen: beti partekatzen dugu [nor garen](https://www.liberaforms.org/es/proyecto) eta [nola egiten dugun lan](https://www.liberaforms.org/es/documentacion), eta [zure iruzkinak](mailto:info@liberaforms.org) beti dira ongietorriak.

### Pribatutasun-laguntzailea

Arreta berezia merezi du gure pribatutasun-laguntzaileak, pribatutasun-adierazpenak idazterakoan irizpide egokienak erabiltzen lagunduko dizunak.

Laguntzaile honek sortutako pribatutasun-adierazpenak ahalik eta borondate onenarekin egin dira, baina ez dira juridikoki lotesleak.

Kontuan hartu:

* Laguntzaileak emandako testua gomendio bat baino ez da, eta ematen duzun informazioaren araberakoa izango da haren balioa.

* Gomendatzen dizugu gure gomendioak eta lege-testuak zure konfiantzako legezko sail edo profesionalarekin berrikus ditzazula, zure tokiko legeriara egokitu ahal izateko.

## 4. Kontuaren baldintzak

Erregistratutako erabiltzailea bere kontuaren eta zerbitzua erabiltzeko behar den pasahitzaren segurtasunaz arduratuko da, baita pasahitza gordetzeaz ere. LiberaFormsek ezin du izan eta ez da izango inolako galeraren edo kalteren erantzule segurtasun-betebehar hau urratu bada.

Pasahitza galdu badu edo lapurtu badiote, erabiltzaileak bere gain hartu beharko ditu ondorioak.

Ez duzu baimenduta gure zerbitzua erabiltzea legez kontrako helburuetarako edo zure jurisdikzioko legeak urratzeko.

Erregistratutako erabiltzailea bere kontuan egiten den guztiaren arduradun da, kontua atzitu dezaketen beste batzuk egiten badute ere.

Gizakia izan behar duzu. Ez da onartzen kontuak bot-en bidez edo bestelako metodo automatizatuez sortzea.

LiberaFormsek hitzematen du erabiltzaileak kontratatutako zerbitzuak ahalik eta lasterren jarriko dituela haren eskura. Ordainpeko modalitateetan, denbora hori 30 minutu eta 96 lanordu inguru artekoa izango da, eskaera behar bezala beteta jasotzen denetik aurrera.

LiberaFormsek ez badu zerbitzua erabiltzailearen eskura jarri 96 lanordu igarotakoan, erabiltzaileak kontratua deuseztatu dezake eta, dirurik ordaindu badu, ordaindutako guztia bueltan jasotzeko eskubidea dauka.

LiberaFormsek erabiltzailearen esku jartzen ditu tresnen erabilera errazteko informazio teknikoa eta argibideak, baita ordainpeko zerbitzuetan eskuragai dauden laguntza-kanal batzuk ere, kontratatutako modalitatearen arabera.

## 5. Mantentze-lanen baldintzak

* Zerbitzua monitorizatua da eta 5 minutu baino gehiagoko intzidentziek abisua ematen dute, ahalik eta lasterren begiratzen duguna.

* Zerbitzuan eragina duten 15 minutu baino gehiagoko mantentze-lanek aurretiaz jakinaraziko ditugu.

* Mantentze-lan txikiagoak, zerbitzuan 5 minutu baino gehiagoko eragina izango ez dutela aurreikusten ditugunak, komenigarri denean egingo ditugu.

* Oro har, esku-hartze nagusiak ordutegi jakin honetan egiten saiatuko gara: astelehenetik ostegunera 18:00 eta 20:00 artean, edo ostiraletan 15:00etatik aurrera, Bartzelonako ordu-zonan.

## 6. Ordainketa, diru-itzultze, aldatze eta degradazio baldintzak

Doako modalitatearen baldintzak izena ematen duzunean azaltzen dizkizugu. Ez dizugu banku-txartelik eskatzen eta, gure zerbitzuengatik ordaintzen duten bezeroekin egiten dugun bezala, ez ditugu zure datuak saltzen.

Ordainpeko zerbitzurik kontratatzen baduzu, ordainketa aldez aurretik egin behar duzu zerbitzua erabiltzen jarraitzeko. Bestela, zure kontua blokeatuko dugu eta ezingo duzu bertara sartu ordainketa egin arte. Zure erregistratutako erabiltzaile-kontua 60 egunez blokeatuta egon bada, automatikoki ezabatzeko ilaran jarriko dugu.

Doako modalitatetik ordainpeko batera aldatzen bazara, berehala kobratuko dizugu eta zure fakturazio-zikloa aldaketaren egunean bertan hasiko da.

Erregistratutako erabiltzaileak hautatutako zerbitzua automatikoki berritzen da baldintza berberekin, zerbitzua berritzeko ordaintzen duen aldiro.

Ordainpeko modalitatean kontratatzen den galdetegietan jasotako erantzun kopurua baliozko epearen barruan erabili behar dira, eta epearen amaieran galdu egingo da erabili ez diren erantzunak jasotzeko aukera.

Inoiz ez zaizu aparteko ezer kobratuko ustekabeko erantzun gailurrengatik. Ez dago ezusteko tarifarik eta ez zaizu ustekabean fakturatuko. Galdetegietan jasotako erantzunak elkarren segidako bi hilabetez ordainpeko modalitatean kontratatutakoa baina gehiago izatekotan, zurekin harremanetan jarriko gara hurrengo hilabeterako modalitate handiago batera aldatzeko. Bi aste izango dituzu erabakia hartzeko. Modalitate handiago batekin jarraitu edo orduantxe bertan zure kontua ixtea erabaki dezakezu.

Noiznahi aldatu edo degradatu dezakezu modalitatea mailaz. Horretarako zuzenean gurekin jarri behar duzu harremanetan. Modalitatea degradatzean ez duzu funtzionalitaterik galduko eta ez du zure kontuan eragingo, ez bada espresuki sortutako instantzia bat zeneukala; kasu horretan datu-basearen kopia bat emango dizugu. LiberaForms ez da beste ezeren erantzule.

Noiznahi itxi dezakezu zure kontua. Zure kontua ixtean betirako galduko dira zure datu guztiak.

Jada ordaindutako tarifak ez dira itzuliko.

Internet bidez OpenCollective plataforman ordaintzeko prozesua Stripe-k egiten du. Bestela, transferentzia bidez edo beste modu batzuetan ordaintzea adostu dezakegu. Tarifa guztiek barne hartzen dituzte zerga-agintariek ezarritako zerga, karga edo eskubide guztiak. OpenCollectivek zerga horiek zerga-agintarien izenean bilduko ditu eta hari emango dizkio. Irakurri OpenCollectiven Erabilera baldintzak xehetasun gehiago edukitzeko.

## 7. Kontua ezabatzea

Edozein arrazoirengatik eta edonoiz zure kontua eten edo itxi dezakegu, eta baita orain edo etorkizunean zerbitzua erabiltzea galarazi ere. Zerbitzua horrela moztean zure kontua itxi edo ezabatuko dugu eta, ondorioz, ezingo dituzu zure galdetegiak, erantzunak eta estatistikak atzitu.

Espainiar Estatuaren Informazioaren Gizarteko Zerbitzuei eta Merkataritza Elektronikoari buruzko 34/2002 Legean ezarritakoaren arabera, LiberaForms behartuta egongo da formalki eskatzen dion erakunde judizialarekin lankidetzan aritzera.

## 8. Zerbitzu- eta prezio-aldaketak

Zerbitzuaren edozein zati aldatu edo eten ahal izango dugu edonoiz, aldi baterako edo betiko, aldez aurretik jakinarazita edo jakinarazi gabe.

LiberaFormsek edonoiz aldatu ahal izango ditu prezioak. Prezio-aldaketek ezin izango dute atzeraeraginik izan eta kontratua lehen aldiz berritzen denetik aurrera soilik aplikatuko dira. Hori egitekotan, behintzat 30 egun lehenago jakinaraziko dizugu, eta erregistratutako erabiltzailea zaren heinean aldaketak onartu edo zerbitzuari uko egin ahalko diozu. Erregistratutako posta elektroniko helbidean jakinaraziko dizugu. Gure blogean edo kaltetutako zerbitzuetan argitaratu dezakegu ere abisua.

LiberaberaForms ez da erantzule, ez zure aurrean, ez hirugarrenen aurrean, zerbitzuaren edozein aldaketa, prezio-aldaketa, etete edo etenaldirengatik.


## 9. Edukiaren jabetza, egile-eskubideak eta marka erregistratua

Zeu zara erantzule bakarra zerbitzuan edo horren bitartez edozein eduki edo bestelako material bidaltzen, argitaratzen, helarazten, erakusten edo epostaz bidaltzen duzunean. Hori dela eta, arduraz jokatuko duzula hitzematen duzu eta zure gain hartzen dituzu zure argitalpenen ondoriozko erantzukizun guztiak.

Ez dugu jabetza intelektualeko eskubiderik erreklamatzen zerbitzuan argitaratzen duzun materialen gainean. Guneko datu guztiak zureak dira.

Zerbitzuaren inguruko iruzkinak, iradokizunak eta ideiak bidali ahal dizkiguzu. Kasu horretan, onartzen duzu eskubide osoa dugula zure iruzkinak, edonola bidali dituzula ere, etortzekoak diren zerbitzu-aldaketetan erabiltzeko eta sartzeko; ez dizugu zertan horregatik ordaindu edo ideiaren egiletza aitortu.

Ez duzu baimenik LiberaFormsen kide zarela adierazteko, benetan kide ez bazara.

## 10. Irudi-eskubideak, ohorea eta intimitatea

Tokian tokiko legeria gehienek Nork bere irudia izateko eskubide aitortzen dutenez, grabatutako edo fotografiatutako pertsona guztiek ezinbestez eman behar dizute eskubidea LiberaFormsen zerbitzuaren bidez bideo edo argazki horiek argitaratzeko.  Eskubide horiek sortutako galdetegiaren helburu jakinera zedarritu behar dira.

Bideo edo irudi horiek ezingo dute bertan agertzen diren pertsonen intimitatea berariaz urratu.

## 11. Zure datuen pribatutasuna eta segurtasuna

Babeskopiak, erredundantziazko kontrolak eta enkriptatzea egiten ditugu zure datuak babestu eta bermatzeko. Galdetegiak internet bidez egiteko gure zerbitzua erabiltzean, LiberaFormsek zure galdetegien eta erantzunen gaineko oinarrizko informazioa modu erantsian jasotzen du, zerbitzuaren funtzionamendu egokia bermatzeko.

Zureak dira zeure galdetegietako eta haien erantzunetako datu guztien eskubideak, tituluak eta ondasunak. Ez dugu biltzen ez analizatzen zure galdetegietako erabiltzaileen informazio pertsonalik, ez dugu haien jokabideari buruzko informaziorik erabiltzen eta ez diegu saltzen ez partekatzen zure galdetegien eta haien erantzunen daturik hirugarrenei.

Dagozkion lege guztiak betetzea onartzen duzu, pribatutasunari eta datuen babesari buruzko arau guztiak barne.

Onartzen duzu zerbitzua ez erabiltzea informazio konfidentziala bidaltzeko, informazio hori baimenik gabe zabaltzeak kalte edo inpaktu material, larri edo katastrofikoa eragin badiezaioke datu-subjektu edo hirugarren bati. Informazio konfidentzialaren barne daude, besteak beste, banku-txartelen informazioa, pasaporte zenbakiak, gobernuak emandako identifikazio zenbakiak, finantza-kontuen informazioa, geokokalekua edo identifikazio pertsonalerako edozein informazio-sistema.

Idatzi hona [datuen pribatutasunari eta segurtasunari](https://www.liberaforms.org/es/politica-de-privacidad) buruz gehiago jakiteko: [info@liberaforms.org](mailto:info@liberaforms.org)

## 12. Baldintza orokorrak

Zure ardurapean erabiltzen duzu LiberaForms. Zerbitzua "bere horretan" eta "prestasunaren arabera" eskaintzen da.

Zerbitzuak tentuz diseinatzen ditugu, geure esperientzian eta haien denbora eta iruzkinak partekatzen dituzten erabiltzaileen esperientzian oinarrituta. Hala ere, ez da guztien gustuko zerbitzurik. Ez dugu bermatzen gure zerbitzuak zure betebeharrak edo espektatiba zehatzak asetzea.

Horrez gain, gure zerbitzuak eskaini aurretik funtzionalitate guztiak sakonki probatzen ditugu. Edozein softwarek bezala, gure zerbitzuek ezinbestean dituzte erroreak. Jakinarazi dizkiguten erroreen jarraipena egiten dugu eta lehentasunezkoak aztertzen ditugu, bereziki segurtasunarekin eta pribatutasunarekin lotura duten horiek. Ez ditugu jakinarazitako errore guztiak konponduko eta ez dugu errorerik gabeko zerbitzurik bermatzen.

Erabiltzaileek onartzen dute zerbitzuko gorabeheren, aldaketen eta nobedadeen berri izatea, erregistratutako erabiltzaileei bidalitako zirkularren bidez.

Ordainpeko modalitatean laguntza teknikoa posta elektronikoz ematen da. Ez da bermatzen denbora-tarte zehatz baten barruan erantzungo denik posta elektronikoz.

## 13. Harremanetarako

Zerbitzuaren baldintzen inguruko galderarik badaukazu, [jarri gurekin harremanetan](mailto:info@liberaforms.org).

Azken aldaketa: 2023ko urriaren 23a.

