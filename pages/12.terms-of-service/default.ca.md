---
title: 'Termes del servei'
published: true
metadata:
    keywords: 'termes del servei, condicions d''ús, LiberaForms'
    description: 'Condicions d''ús del servei SaaS de LiberaForms'
slug: termes-del-servei
visible: false
---

# Condicions d'ús del servei SaaS de LiberaForms
Gràcies per fer ús d’aquesta instància dedicada sota el servei al núvol de LiberaForms!

## 1. Identificació del responsable del servei
LiberaForms és una iniciativa col·lectiva coproduïda amb el suport de la comunitat de programari lliure per al desenvolupament de formularis ètics en línia. Pots saber més sobre el projecte a [https://liberaforms.org/ca/projecte](https://liberaforms.org/ca/projecte) 

El present servei al núvol, conegut com a _Software as a Service_ (SaaS), de LiberaForms s’ofereix de  forma finalista com a alternativa a la instal·lació de la teva pròpia instància. El seu objecte és doble: donar a conèixer la solució de formularis ètics de LiberaForms i sufragar els costos de desenvolupament del codi opensource del projecte LiberaForms.

El representant legal d'aquest servei és Estraperlo S.Coop. de C-LM amb domicili social al carrer Matadero, 5 – 02.260 Fuentealbilla (Espanya, Unió Europea) amb identificació fiscal F-02546489, que actua com a representant de les instàncies públiques de LiberaForms sota domini liberaforms.org. 

Quan parlem de "projecte", "nosaltres", "nostre", "servei" o "serveis" en aquest document, ens referim a LiberaForms.

Quan ens referim a les «instàncies públiques» de LiberaForms o a «instàncies dedicades» ens referim al servei SaaS de LiberaForms.

## 2. Definicions
### 2.1. Usuària
Usuària és qualsevol persona que connecti amb una de les webs o sistemes de LiberaForms. Existeixen dos tipus d'usuaris de LiberaForms:

* Usuàries registrades, són les qui han estat convidades, editen o creen formularis de LiberaForms i poden consultar les dades de les usuàries anònimes.
* Usuàries anònimes, són les qui, sense necessitat de registre, responen a algun dels formularis creats des de LiberaForms per les usuàries registrades.

A més, existeix per part de l'equip de LiberaForms:

* Administradores, que gestionen les dades de les usuàries registrades de forma directa, però sense accedir a les dades de respostes de les usuàries anònimes.
* Administradora de sistema, que és l’encarregada del correcte funcionament del servei i pot, si així se sol·licita, accedir a la base de dades.

### 2.2. Serveis
El servei SaaS que LiberaForms ofereix inclou serveis digitals de plataformes en línia.

Existeixen dos tipus de serveis: serveis gratuïts a disposició de tota la comunitat i serveis de pagament.

LiberaForms busca facilitar el nombre més gran de persones que s’adonin que poden recuperar el control sobre les seves dades. Per això, estan a disposició de tota la comunitat les instàncies públiques llistades a [https://www.liberaforms.org/](https://www.liberaforms.org/ca) en les seves modalitats gratuïtes. Aquesta modalitat inclou l'ús de totes les funcionalitats, però limita el seu ús a un volum de respostes permeses per any natural.

Addicionalment, per les usuàries registrades en aquestes instàncies amb necessitats de major volum de respostes anuals o per les usuàries que requereixen una instància  dedicada existeixen serveis de pagament que es detallen a la web de LiberaForms per a la seva contractació sota [https://liberaforms.org/ca/serveis](https://liberaforms.org/ca/serveis) 

### 2.3. Quotes
Es refereix al preu dels serveis que una usuària registrada contracta amb LiberaForms.

### 2.4. Canals de suport
LiberaForms ofereix un conjunt de serveis per facilitar l'experiència de la usuària als quals anomenem canals de suport i que són, per exemple, la documentació tècnica i d'usuària, el suport comunitari entre usuàries o el correu electrònic, per a l'atenció individualitzada.

## 3. Termes del servei
LiberaForms proporciona el servei subjecte al compliment per part de les usuàries d’uns termes i condicions específics.

**SI US PLAU, LLEGEIX AQUEST ACORD AMB CURA ABANS D’ACCEDIR AL SERVEI. EN ACCEDIR AL SERVEI ACCEPTES ESTAR OBLIGADA PELS TERMES I CONDICIONS QUE AQUÍ ES DETALLEN.**

Aquí s'indiquen les condicions jurídiques, tècniques i econòmiques que regulen la prestació de serveis entre LiberaForms i les seves usuàries.

Els següents  termes del servei s'apliquen únicament a les instàncies dedicades sota el servei SaaS de LiberaForms publicades a https://liberaforms.org. No s'aplica a les instàncies autoallotjades del programari LiberaForms, que estarien allotjades en els seus propis servidors i, per tant, els termes del servei no són necessaris.

És possible que actualitzem aquests termes del servei en el futur. Cada cop que fem un canvi significatiu a les nostres polítiques, ho anunciarem al nostre [blog](https://blog.liberaforms.org/).

Les tarifes generals es troben disponibles a la pàgina web [https://www.liberaforms.org/ca/serveis](https://www.liberaforms.org/ca/serveis)

La usuària registrada i LiberaForms poden pactar puntualment tarifes per a serveis especials que no apareixen entre els  que s’ofereixen per LiberaForms al seu lloc web.

La violació dels termes llistats a continuació pot resultar en la cancel·lació del teu compte. 

Aquesta és una declaració àmplia i implica que has de confiar molt en nosaltres. Fem tot el possible per merèixer aquesta confiança compartint en tot moment [qui som](https://www.liberaforms.org/ca/projecte), [com treballem](https://docs.liberaforms.org/) i mantenint una porta oberta als [teus comentaris](mailto:info@liberaforms.org).

### Assistent de privacitat 
Especial menció al nostre assistent de privacitat que us ajudarà a fer ús dels criteris més adients a l'hora d'escriure les vostres declaracions de privadesa.

Les declaracions de privadesa generades per aquest assistent estan fetes amb la millor intenció, tanmateix no són legalment vinculants.
Si us plau, tingues en compte que:

* 	El text proporcionat per l'assistent és només una recomanació i la seva validesa dependrà de la informació que proporcionis. 
* 	Et recomanem que consultis les nostres recomanacions i textos legals amb el teu departament jurídic o amb un professional de confiança, perquè puguis adaptar-los a la teva legislació local.

## 4. Termes del teu compte
La usuària registrada és responsable de mantenir la seguretat del seu compte i de la contrasenya necessària per a la utilització del servei així com el seu emmagatzematge. LiberaForms no pot ser ni serà responsable de cap pèrdua o dany que derivi de l’incompliment d'aquesta obligació de seguretat.

La usuària registrada haurà d'assumir íntegrament les conseqüències en cas de pèrdua o robatori de les seves contrasenyes.

La usuària registrada no pot utilitzar el nostre servei per a cap propòsit il·legal o per violar les lleis de la teva jurisdicció.

La usuària registrada és responsable de qualsevol activitat que ocorri al seu compte (també quan siguin altres els qui iniciïn sessió al seu compte).

Cal que siguis humà. No es permet el registre de comptes mitjançant bots o altres mètodes automatitzats.

LiberaForms es compromet a posar a disposició de la usuària els serveis contractats en el menor temps possible. En les modalitats de pagament, s'estima aquest temps entre 30 minuts i 96 hores laborables des de la recepció de la petició correctament complimentada.

Si LiberaForms no ha posat a disposició de la usuària registrada el servei en un termini màxim de 96 hores laborables aquesta tindrà dret a rescindir el contracte i al reemborsament íntegre de la suma pagada, si aquesta s'hagués realitzat.

LiberaForms posa a disposició de la usuària d’un conjunt d'informació tècnica i guies per facilitar l'ús de les eines així com una sèrie de canals de suport disponibles per a serveis de pagament, segons la modalitat contractada.

## 5. Termes del manteniment
* 	El servei est monitoritzat i les incidències de més de 5 minuts generen una alerta que serà analitzada a la major brevetat. 
* 	Les tasques de manteniment que afectin el servei durant més de 15 minuts es comunicaran prèviament.
* 	Labors de manteniment menors, que no prevegin afectació de servei durant més de 5 minuts es realitzaran quan sigui convenient.
* 	En general, s'intentaran programar les actuacions més grans perquè passin dilluns a dijous entre les 18 i les 20, o els divendres a partir de les 15, hora de Barcelona.

## 6. Termes de pagament, reemborsament, actualització i degradació
Per a la nostra modalitat gratuïta, t’expliquem les condicions del servei quan us registreu. No et demanem cap mitjà de pagament i, igual que per als clients que pagant pels nostres serveis, no venem les teves dades.

Si contractes qualsevol servei de pagament, has d’efectuar el pagament per avançat per continuar utilitzant el servei. En cas contrari, congelarem el teu compte i et serà inaccessible fins que s’efectuï el pagament. Si el teu compte d'usuària registrada ha estat congelat durant 60 dies, serà posada en cua per a la seva eliminació automàtica.

Si actualitzes el seu servei des de la modalitat gratuïta a un pla de pagament, et cobrarem de manera immediata i el teu cicle de facturació començarà el dia de l'actualització.

El servei escollit per la usuària registrada es renova sota les mateixes condicions a cada pagament que s'efectua per perllongar el servei.

La quantitat de respostes a formularis contractats en el pla de pagament s'ha de fer servir durant el termini aplicable, i les respostes a formularis no fetes servir al final del termini es perdran.

Mai se’t cobrarà per un pic de respostes puntual. No hi ha tarifes sorpresa i no se’t facturarà res inesperat. En cas que l'ús de respostes a formularis excedeixi durant dos mesos consecutius el pla de pagament escollit, ens posarem en contacte amb tu per actualitzar-te a un pla superior per al mes següent. 
Tindràs dues setmanes per prendre una decisió. Pots decidir continuar amb un pla superior o cancel·lar el teu compte en aquest moment.

Pots actualitzar o degradar la modalitat del teu pla en qualsevol moment. Per fer-ho, cal que contacteu amb nosaltres directament. La degradació del teu pla no causa la pèrdua de funcions del teu compte, excepte si implica la renúncia a una instància dedicada, en aquest cas et facilitarem una còpia de la base de dades. LiberaForms no accepta cap altra responsabilitat.

Pots cancel·lar el teu compte en qualsevol moment. La cancel·lació del teu compte causa la pèrdua irreversible de totes les teves dades.

En els casos de degradació o cancel·lació a petició de la usuària registrada, les quantitats ja pagades no són reemborsables.

El nostre procés de pagament en línia a través d'[OpenCollective](https://opencollective.com/liberaforms) és a càrrec de Stripe. Les tarifes inclouen tots els impostos o gravàmens segons les autoritats fiscals. OpenCollective recaptarà aquests impostos en el nom de l'autoritat fiscal i els hi farà arribar directament. Consulta els termes d'ús d'[OpenCollective](https://opencollective.com/tos) per obtenir més detalls. Alternativament, es podran acordar pagament mitjançant transferència o altres vies a convenir. 

## 7. Eliminació del compte
Ens reservem el dret de suspendre o cancel·lar el teu compte i rebutjar qualsevol ús actual o futur del servei per qualsevol motiu i en qualsevol moment. Aquesta finalització del servei resultarà en la desactivació o eliminació del teu compte i, per tant, no podràs accedir als teus formularis, respostes i estadístiques.

En virtut d’allò que estableix la Llei 34/2002 de Serveis de la Societat de la Informació i del Comerç Electrònic de l'Estat espanyol, LiberaForms es veu obligada a cooperar amb l'organisme judicial que li ho sol·liciti formalment.

## 8. Modificacions en el servei i preus
Ens reservem el dret de modificar o discontinuar en qualsevol moment, temporalment o permanentment, qualsevol part del servei amb previ avís o sense.

LiberaForms podrà actualitzar els preus en qualsevol moment. Les modificacions dels preus no podran tenir caràcter retroactiu i seran aplicables només a partir de la primera renovació del contracte. Si ho fem, us donarem un avís mínim 30 dies abans i com a usuària registrada tens la potestat d'acceptar o renunciar al servei. Et notificarem a través de l’adreça de correu electrònic registrada. També podem publicar un avís sobre canvis al nostre blog o als mateixos serveis afectats.

LiberaForms no serà responsable davant cap tercer per qualsevol modificació, canvi de preu, suspensió o interrupció del servei.

## 9. Propietat del contingut, drets d'autor i marca
Tu ets l’única responsable de qualsevol contingut i altre material que enviïs, publiquis, transmetis, enviïs per correu electrònic o mostris en, a través o amb el servei. Per això, et compromets a fer un ús responsable i assumeixes totes les responsabilitats derivades del teu comportament.

No reclamem drets de propietat intel·lectual sobre el material que publiquis en el servei. Totes les dades facilitades continuen sent teves.

En podeu fer arribar comentaris, suggeriments i idees sobre el servei. En aquest cas, acceptes que tenim tots els drets per utilitzar i incorporar els comentaris que ens proporciones de qualsevol manera, inclosa la incorporació a futures millores i modificacions del servei, sense compensació econòmica o obligació de fer atribució a la teva persona.

No t’és permès indicar falsament que estàs associada amb LiberaForms, excepte acord per escrit en aquest sentit.

## 10. Drets d’imatge, honor i a la intimitat
Atès que el Dret a la pròpia imatge és un dret reconegut en la majoria de legislacions locals, és indispensable disposar de la cessió dels drets d’imatge de totes les persones enregistrades o fotografiades per poder publicar les fotografies i vídeos a través del servei de LiberaForms. Aquests drets han d’estar circumscrits a l’ús concret al qual es destina el formulari creat.

Aquestes imatges i/o vídeos no podran afectar expressament a la intimitat de les persones que hi apareixen.

## 11. Privacitat i seguretat de les teves dades
Prenem mesures per protegir i assegurar les teves dades a través de còpies de seguretat, controls per redundàncies i encriptació. Quan fas servir el nostre servei per gestionar els teus formularis en línia, LiberaForms recopila informació mínima i de forma agregada sobre el teu ús de formularis i respostes per garantir el correcte funcionament del servei.

Tens tots els drets, títols i interessos de les dades dels teus formularis i les seves respostes. No recollim ni analitzem informació personal de les usuàries anònimes dels teus formularis, no utilitzem informació sobre el seu comportament i no venem ni compartim les dades dels teus formularis o les seves respostes amb tercers.

Acceptes complir amb totes les lleis aplicables, incloses totes les normes de privadesa i protecció de dades sota la teva jurisdicció.

Acceptes no utilitzar el servei per enviar informació confidencial on la divulgació no autoritzada podria causar un dany o impacte material, greu o catastròfic a qualsevol subjecte de dades o a tercers. La informació confidencial inclou, entre altres, informació de targetes bancàries, números de passaport, números d'identificació emesos pel govern, informació de comptes financers, geolocalització o qualsevol sistema d'informació d'identificació personal.

Podeu contactar amb nosaltres aquí per informar-te sobre la nostra política de [protecció i privacitat de dades](https://liberaforms.org/ca/politica-de-privacitat): [info@liberaforms.org ](mailto:info@liberaforms.org)

## 12. Condicions generals
Uses LiberaForms sota el teu propi risc. El servei es proporciona "tal com és" i "segons disponibilitat".

Dissenyem els nostres serveis amb cura, basats en la nostra pròpia experiència i les experiències de les usuàries que comparteixen el seu temps i els seus comentaris.  Tanmateix, no existeix un servei que agradi a tothom. No et podem garantir que els nostres serveis compleixin amb els seus requisits o expectatives específiques.

També  fem proves exhaustives de totes les nostres funcions abans de desplegar-les. Com en qualsevol programari, els nostres serveis tenen inevitablement alguns errors. Realitzem un seguiment dels errors que ens informem i analitzem els prioritaris, especialment els relacionats amb la seguretat o la privadesa. No tots els errors informats se solucionaran i no garantim serveis completament lliures d'errors.

Les usuàries accepten rebre notificacions sobre incidències, canvis i novetats al servei a través de les circulars  per usuàries registrades.
En la modalitat de pagament, el suport tècnic es proporciona per correu electrònic. Les respostes per correu electrònic es proporcionen sense un temps de resposta garantida.

## 13. Contacta'ns
Si  tens qualsevol dubtes sobre els Termes del servei, [comunica't amb nosaltres](mailto:info@liberaforms.org).

_Darrera actualització: 23 d'octubre de 2023_