---
title: 'Términos del servicio'
visible: false
slug: terminos-del-servicio
---

# Condiciones de uso del servicio SaaS de LiberaForms
¡Gracias por usar esta instancia dedicada bajo el servicio en la nube de LiberaForms!

## 1. Identificación del responsable del servicio
LiberaForms es una iniciativa colectiva que se apoya en la comunidad de software libre para el desarrollo de formularios éticos en línea.
Puedes saber más sobre el proyecto en [https://www.liberaforms.org/es/proyecto](https://www.liberaforms.org/es/proyecto) 

El presente servicio en la nube, conocido como _Software as a Service_ (SaaS), de LiberaForms se ofrece de forma finalista como alternativa a la instalación de tu propia instancia. Su objetivo es doble: dar a conocer la solución de formularios éticos de LiberaForms y sufragar los costes de desarrollo del código opensource del proyecto LiberaForms.

El representante legal de este servicio es Estraperlo S.Coop. de C-LM con domicilio social en calle Matadero, 5 – 02.260 Fuentealbilla (España, Uni) con identificación fiscal F-02546489, que actúa como representante de las instancias públicas de LiberaForms bajo dominio liberaforms.org.

Cuando decimos "proyecto", "nosotros", "nuestro", "servicio" o "servicios" en este documento, nos referimos a LiberaForms.

Cuando nos referimos a las «instancias públicas» o a «instancias dedicadas» nos referimos al servicio SaaS de LiberaForms.

## 2. Definiciones
### 2.1. Usuaria
Usuaria es cualquier persona que conecta con una de las webs o sistemas de LiberaForms. Existen dos tipos de usuarias de LiberaForms:
* Usuarias registradas, que son las que son invitadas, editan o crean formularios de LiberaForms y pueden consultar los datos de las usuarias anónimas.
* Usuarias anónimas, que son las que, sin necesidad de registro, responden a alguno de los formularios creados desde LiberaForms por las usuarias registradas.

Adicionalmente, existen por parte del equipo de LiberaForms:
* Administradoras, que gestionan los datos de los usuarios registrados de forma directa, pero sin acceso a los datos de respuestas de los usuarios anónimos.
* Administradora de sistema, que se encarga del correcto funcionamiento del servicio y puede, bajo petición, acceder a la base de datos.

### 2.2. Servicios
El servicio SaaS que LiberaForms ofrece incluye servicios digitales de plataformas en línea.

Existen dos tipos de servicios: servicios gratuitos a disposición de toda la comunidad de usuarias y servicios de pago.

LiberaForms busca facilitar al máximo de gente el uso de los servicios gratuitos para favorecer que se den cuenta de que pueden recuperar el control sobre sus datos. Por ello, están a disposición de toda la comunidad las instancias públicas listadas en [https://www.liberaforms.org/](https://www.liberaforms.org/es) en sus modalidades gratuitas. Dicha modalidad incluye el uso de todas las funcionalidades, pero limita su uso a un volumen de respuestas permitidas por año natural.

Adicionalmente, para las usuarias registradas en estas instancias con necesidades de mayor volumen de respuestas anuales o para las usuarias que requieran una instancia propia dedicada, existen servicios de pago que se ofrecen detallados en la web de LiberaForms para su contratación bajo [https://www.liberaforms.org/es/servicios](https://www.liberaforms.org/es/servicios)

### 2.3. Cuotas
Se refiere al precio de los servicios que una usuaria contrata con LiberaForms.

### 2.4. Canales de soporte
LiberaForms ofrece un conjunto de servicios para facilitar la experiencia de usuaria, a los que llamamos canales de soporte, y que son, por ejemplo, la documentación técnica y de usuaria, el soporte comunitario entre las usuarias o el correo electrónico, para la atención individualizada.

## 3. Términos del servicio
LiberaForms proporciona a las usuarias el servicio sujeto al cumplimiento por parte de las usuarias de unos términos y condiciones específicos.

**POR FAVOR LEE ESTE ACUERDO CUIDADOSAMENTE ANTES DE ACCEDER AL SERVICIO. AL ACCEDER AL SERVICIO, ACEPTAS ESTAR OBLIGADA POR LOS TÉRMINOS Y CONDICIONES QUE AQUÍ SE DETALLAN**

Aquí se indican las condiciones jurídicas, técnicas y económicas que regulan la prestación de servicios entre LiberaForms y las usuarias.

Los siguientes Términos del servicio se aplican únicamente a las instancias dedicadas bajo el servicio SaaS de LiberaForms publicadas en [https://www.liberaforms.org](https://www.liberaforms.org/es). No se aplican a las instancias autoalojadas del software LiberaForms, que estarían alojadas en sus propios servidores y, por lo tanto, los Términos del servicio no son necesarios.

Es posible que actualicemos estos Términos del servicio en el futuro. Cada vez que hagamos un cambio significativo en nuestras políticas, lo anunciaremos en nuestro [blog](https://blog.liberaforms.org/).

Las tarifas generales se encuentran disponibles en la página web [https://www.liberaforms.org/es/servicios](https://www.liberaforms.org/es/servicios).

La usuaria y LiberaForms pueden pactar puntualmente tarifas para servicios especiales que no aparezcan entre los ofrecidos por LiberaForms en su sitio web.

La violación de los términos listados a continuación puede resultar en la cancelación de tu cuenta. 

Esta es una declaración amplia y significa que debes confiar mucho en nosotros. Hacemos todo lo posible para merecer esa confianza, compartiendo en todo momento [quiénes somos](https://www.liberaforms.org/es/proyecto), [cómo trabajamos](https://docs.liberaforms.org/) y manteniendo una puerta abierta a [tus comentarios](mailto:info@liberaforms.org).

### Asistente de privacidad
Especial atención a nuestro asistente de privacidad que te ayudará a hacer uso de los criterios más adecuados a la hora de escribir tus declaraciones de privacidad.

Las declaraciones de privacidad generadas por este asistente están hechas con la mejor intención, sin embargo no son legalmente vinculantes.

Por favor, ten en cuenta que:

* 	El texto proporcionado por el asistente es solo una recomendación y su validez dependerá de la información que proporciones. 
* 	Te recomendamos que consultes nuestras recomendaciones y textos legales con tu departamento jurídico o con un profesional de confianza, para que puedas adaptarlos a tu legislación local.


## 4. Términos de la cuenta
La usuaria registrada es responsable de mantener la seguridad de su cuenta y de la contraseña necesaria para la utilización del servicio, así como de su almacenamiento. LiberaForms no puede ser y no será responsable de ninguna pérdida o daño por el incumplimiento de esta obligación de seguridad.

La usuaria deberá asumir íntegramente las consecuencias en caso de perdida o robo de sus contraseñas.

No puedes usar nuestro servicio para ningún propósito ilegal o para violar las leyes de tu jurisdicción.

La usuaria registrada es responsable de cualquier actividad que ocurra en su cuenta (incluso por parte de otros que tengan sus propios inicios de sesión en su cuenta).

Debes ser un humano. No se permite el registro de cuentas mediante bots u otros métodos automatizados.

LiberaForms se compromete a poner a disposición de la usuaria los servicios contratados en el menor tiempo posible. En las modalidades de pago, se estima que este tiempo sea de entre 30 minutos y 96 horas laborables desde la recepción del pedido correctamente cumplimentado.

Si LiberaForms no ha puesto a disposición del cliente el servicio en un plazo máximo de 96 horas laborables, la usuaria tendrá derecho a rescindir el contrato y al reembolso íntegro de la suma pagada, si esta se hubiera realizado ya.

LiberaForms pone a disposición de la usuaria un conjunto de información técnica y guías para facilitar el uso de las herramientas, así como una serie de canales de soporte disponibles para servicios de pago, según modalidad contratada.

## 5. Términos del mantenimiento

* El servicio está monitorizado y las incidencias de más de 5 minutos generan una alerta que será analizada a la mayor brevedad.
* Las tareas de mantenimiento que afecten al servicio durante más de 15 minutos se comunicarán previamente.
* Labores de mantenimiento menores, que no prevean afectación de servicio durante más de 5 minutos, se realizarán cuando sea conveniente.
* En general, se intentarán programar las mayores actuaciones para que pasen de lunes a jueves entre las 18 y las 20, o los viernes a partir de las 15, hora de Barcelona.

## 6. Términos de pago, reembolsos, actualización y degradación
Para nuestra modalidad gratuita, te explicamos las condiciones de dicha modalidad cuando te registras. No te pedimos tu tarjeta bancaria y, al igual que para los clientes que pagan por nuestros servicios, no vendemos tus datos. 

Si contratas cualquier servicio de pago, debes realizar el pago por adelantado para seguir usando el servicio. En caso contrario, congelaremos tu cuenta y te será inaccesible hasta que se realice el pago. Si tu cuenta de usuaria registrada ha estado congelada durante 60 días, la pondremos en cola para su eliminación automática.

Si actualizas desde la modalidad gratuita a un plan de pago, te cobraremos de inmediato y tu ciclo de facturación comenzará el día de la actualización.

El servicio elegido por la usuaria registrada se renueva bajo las mismas condiciones a cada pago que efectúa para prolongar el servicio.

La cantidad de respuestas a formularios contratadas en el plan de pago deben usarse durante el plazo aplicable, y las respuestas a formularios no utilizadas al final del plazo se perderán.

Nunca se te cobrará extra por un pico de respuestas ocasional. No hay tarifas sorpresa y no se te facturará inesperadamente. En caso de que el uso de respuestas a formularios exceda durante dos meses consecutivos el plan de pago que seleccionaste, nos pondremos en contacto contigo para actualizar a un plan superior para el mes siguiente. Tendrás dos semanas para tomar una decisión. Puedes decidir continuar con un plan superior o cancelar tu cuenta en ese momento.

Puedes actualizar o degradar el nivel del plan en cualquier momento. Para ello debes contactar con nosotros directamente. La degradación de tu plan no causa la pérdida de funciones de tu cuenta, salvo si tenías una instancia dedicada, en cuyo caso te facilitaremos una copia de la base de datos. LiberaForms no acepta ninguna otra responsabilidad.

Puedes cancelar tu cuenta en cualquier momento. La cancelación de tu cuenta causa la pérdida irreversible de todos los tus datos.

Las tarifas ya pagadas no son reembolsables.

Nuestro proceso de pago en línea a través de OpenCollective lo realiza Stripe. Alternativamente, podemos acordar pago mediante transferencia u otras vías a convenir. Todas las tarifas incluyen todos los impuestos, gravámenes o derechos impuestos por las autoridades fiscales. OpenCollective recaudará esos impuestos en nombre de la autoridad fiscal y se los remitirá a ella. Consulta los Términos de uso de OpenCollective para obtener más detalles.

## 7. Eliminación de la cuenta
Nos reservamos el derecho de suspender o cancelar tu cuenta y rechazar cualquier uso actual o futuro del servicio por cualquier motivo y en cualquier momento. Dicha terminación del servicio resultará en la desactivación o eliminación de tu cuenta, y por ende, no podrás acceder a tus formularios, respuestas y estadísticas.

En virtud de lo establecido en la Ley 34/2002 de Servicios de la Sociedad de la Información y de Comercio Electrónico del Estado español, LiberaForms se verá obligada a cooperar con el organismo judicial que se lo solicite formalmente.

## 8. Modificaciones en el servicio y precios
Nos reservamos el derecho de modificar o discontinuar en cualquier momento, temporal o permanentemente, cualquier parte del servicio con o sin previo aviso.

LiberaForms podrá actualizar los precios en cualquier momento. Las modificaciones de los precios no podrán tener carácter retroactivo y serán aplicables solo a partir de la primera renovación del contrato. Si lo hacemos, te avisaremos al menos 30 días antes y como usuaria registrada tienes la potestad de aceptarlas o renunciar al servicio. Te lo notificaremos a través de la dirección de correo electrónico registrada. También podemos publicar un aviso sobre cambios en nuestro blog o en los propios servicios afectados.

LiberaForms no será responsable ante ti ni ante ningún tercero por cualquier modificación, cambio de precio, suspensión o interrupción del servicio.

## 9. Propiedad del contenido, derechos de autor y marca registrada
Tú eres la única responsable de cualquier contenido y otro material que envíes, publiques, transmitas, envíes por correo electrónico o muestres en, a través o con el servicio. Por ello, te comprometes a hacer un uso responsable y asumes todas las responsabilidades derivadas de tus publicaciones.

No reclamamos derechos de propiedad intelectual sobre el material que publicas en el servicio. Todos los datos del sitio siguen siendo tuyos.
Puedes proporcionarnos comentarios, sugerencias e ideas sobre el servicio. En este caso, aceptas que poseemos todos los derechos para usar e incorporar los comentarios, que proporciones de cualquier manera, incluso en futuras modificaciones al servicio, sin pago ni atribución a tu persona.

No tienes permitido indicar falsamente que está asociado con LiberaForms.

## 10. Derechos de imagen, honor y la intimidad
Dado que el Derecho a la propia imagen es un derecho reconocido en la mayoría de legislaciones locales, es indispensable disponer de la cesión de los derechos de imagen de todas las personas grabadas o fotografiadas para poder publicar las fotografías y vídeos a través del servicio de LiberaForms. Estos derechos deben estar circunscritos al uso concreto al que se destina el formulario creado.

Estas imágenes y/o vídeos no podrán afectar expresamente a la intimidad de las personas que aparecen.

## 11. Privacidad y seguridad de tus datos
Tomamos medidas para proteger y asegurar tus datos a través de copias de seguridad, redundancia y encriptación. Cuando utilizas nuestro servicio para gestionar tus formularios en línea, LiberaForms recopila información mínima y de forma agregada sobre tu uso de formularios y respuestas para garantizar el correcto funcionamiento del servicio.

Posees todos los derechos, títulos e intereses de los datos de tus formularios y sus respuestas. No recopilamos ni analizamos información personal de los usuarios de tus formularios, no utilizamos información sobre su comportamiento y no vendemos ni compartimos los datos de tus formularios y sus respuestas con terceros.

Aceptas cumplir con todas las leyes aplicables, incluidas todas las normas de privacidad y protección de datos.

Aceptas no utilizar el servicio para enviar información confidencial donde la divulgación no autorizada podría causar un daño o impacto material, grave o catastrófico a cualquier sujeto de datos o a terceros. La información confidencial incluye, entre otros, información de tarjetas bancarias, números de pasaporte, números de identificación emitidos por el gobierno, información de cuentas financieras, geolocalización o cualquier sistema de información de identificación personal.

Puedes contactarnos aquí para informarte sobre la [protección y privacidad de datos](https://www.liberaforms.org/es/politica-de-privacidad): [info@liberaforms.org](mailto:info@liberaforms.org) 

## 12. Condiciones generales
Usas LiberaForms bajo tu propio riesgo. El servicio se proporciona "tal cual es" y "según disponibilidad".
Diseñamos nuestros servicios con cuidado, basados en nuestra propia experiencia y las experiencias de las usuarias que comparten su tiempo y sus comentarios. Sin embargo, no existe un servicio que agrade a todos. No garantizamos que nuestros servicios cumplan con tus requisitos o expectativas específicas.

También probamos exhaustivamente todas nuestras funciones antes de desplegarlas. Como en cualquier software, nuestros servicios tienen inevitablemente algunos errores. Realizamos un seguimiento de los errores que se nos informan y analizamos los prioritarios, especialmente los relacionados con la seguridad o la privacidad. No todos los errores informados se solucionarán y no garantizamos servicios completamente libres de errores.

Las usuarias aceptan recibir notificaciones sobre incidencias, cambios y novedades en el servicio a través de las circulares enviadas a usuarias registradas.

En la modalidad de pago, el soporte técnico se proporciona por correo electrónico. Las respuestas por correo electrónico se proporcionan sin un tiempo de respuesta garantizado.

## 13. Contáctanos
Si tienes alguna pregunta sobre cualquiera de los Términos del servicio, [comunícate con nosotros](mailto:info@liberaforms.org).

_Última actualización: 23 de octubre de 2023_