---
title: Proyecto
published: true
slug: proyecto
---

# El Proyecto
LiberaForms es cultura libre de proximidad y software de kilómetro cero bajo licencia AGPLv3.

* [Valor añadido](mas-valores)
* [Cronología](cronologia)

## El origen
El software se empezó a desarrollar en el 2019 bajo el nombre de GNGforms, pero la idea se tuvo dos años antes. Cuando en el 2017 en el barrio de Sants se estaba luchando para conservar el Hortet de La Farga, se creó un formulario de Google para recoger firmas. Y resultó que no sólo se pagaba con datos: a partir de cierto número de respuestas al formulario, o pagabas o la empresa secuestraba los datos. Y así fue cómo nació la idea de crear unos formularios éticos para que los colectivos del barrio pudiesen recoger datos de forma respetuosa y sin sorpresas. 

## El contexto
Hoy día, casi todos los espacios y proyectos independientemente de si son colectivos, asociaciones, cooperativas o centros educativos, tienen la necessidad de recoger datos para llevar a cabo sus actividades: reservas, consultas, adhesiones a manifiestos, etc. A menudo, por desconocimiento, se hace uso de software privativo de empresas multinacionales que justamente basan su negocio en la venta de nuestros datos a otras empresas para que puedan diseñar publicidades a medida, específicas para cada persona.

A través de la recogida de datos, empresas multinacionales como las GAFAM (Google, Apple, Facebook, Amazon, Microsoft) saben quienes somos, con quién estamos, las cosas que nos preocupan, nuestras alegrías, intereses y planes. Todo eso, ha permitido que sean de las empresas más ricas del planeta y que tengan un poder de manipulación social sin precedentes. Pero ninguno de los productos y servicios de estas grandes empresas son gratis: los pagamos con nuestros datos, que son la materia prima del capitalismo digital imperante.

[Las GAFAM por La Quadrature du net (ca)](https://liberaforms.org/ca/projecte/gafam-quadrature-cat-a4.pdf)

Por otro lado, la ley de protección de datos vigente obliga a recoger los datos de forma respetuosa. Y además, a raíz del Covid-19, hasta es obligatorio hacer reserva online para las actividades culturales. 

## La ética
La concentración de poder en empresas tecnológicas se ha agudizado en los últimos años. Aún así, todavía hay personas que pensamos que las tecnologías digitales son una oportunidad para reducir desigualdades, crear oportunidades y garantizar este horizonte común en una economía realmente social y solidaria, lejos de capitalismos depredadores. Y es por eso que creamos y mejoramos bienes comunes digitales, tecnologías éticas en un contexto de cultura libre.

Construyamos juntas ese horizonte con una ética digital explícita: echemos a las GAFAM de nuestros espacios y proyectos, usemos LiberaForms para confeccionar nuestros formularios!

## Otras herramientas
No somos el único proyecto que ofrece formularios. Si LiberaForms no se ajusta a tus necesidades, quizás te interesa echar un vistazo a otras herramientas libres que han creado otras personas:

* <https://formtools.org>
* <https://www.drupal.org/project/webform>
* <https://yakforms.org/>
* <https://www.limesurvey.org>
* <https://ohmyform.com>
